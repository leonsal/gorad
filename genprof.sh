#!/bin/sh
# To generate a profile execute "gorad -cpuprofile gorad.prof" for
# some time to collect profile data. Then exits "gorad" and
# execute this script to generate the profile diagram in svg.
# For Go1.9+ only the profile file is necessary.
# Use a browser to view the generated svg file.
go tool pprof -svg gorad.prof > gorad.svg


