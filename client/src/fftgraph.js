/**
 * Webix UI Component to show FFT graph using a canvas element
 */
'use strict';
import * as logger from './logger';

// Maps frequency range to frequency cursor step
const cursorStepMap = new Map([
    [ 1e6,  1e3],       // 0      to 1Mhz    -> 1Khz
    [10e6,  10e3],      // 1Mhz   to 10Mhz   -> 10Khz
    [80e6,  50e3],      // 10Mhz  to 80Mhz   -> 50Khz
    [110e6, 100e3],     // 80Mhz  to 110Mhz  -> 100Khz
    [200e6, 1e6],       // 200Mhz to 500Mhz  -> 1.000Mhz
    [10006, 2e6],       // 500Mhz to 1Ghz    -> 2.000Mhz
    [20006, 5e6],       // 2Gz    to ...     -> 5.000Mhz
]);

webix.protoUI({

    name: 'fftgraph',

    // Default configuration values which can be overriden by the user configuration
    defaults: {
        marginTop:          20,                    // Grid top margin width in pixels
        marginBottom:       40,                    // Grid bottom margin width in pixels
        marginLeft:         64,                    // Grid left margin width in pixels
        marginRight:        30,                    // Grid right margin width in pixels
        tickX:              16,                    // Length in pixels of x axis ticks
        tickY:              16,                    // Length in pixels of y axis ticks
        gridX:              8,                     // Number of x grid lines
        gridY:              13,                    // Number of y grid lines
        backgroundColor:    '#101010',             // Background color
        axisColor:          '#505050',             // Axis and grid lines color
        labelFont:          '10pt Helvetica',      // Font for axis labels
        labelColor:         'white',               // Color for axis labels
        graphLineWidth:     1,                     // Width for graph lines
        graphLineColor:     '#FFFFFF',             // Color for graph lines
    },

    // Component initialization
    // config is an object which can override default configurations
    $init: function(/* config */) {

        this.$view.className = 'app_fftgraph';
        // Creates canvas element and appends to this view
        this._canvas = document.createElement('canvas');
        this._canvas.addEventListener('mousemove',  (ev) => this._onMouseMove(ev));
        this._canvas.addEventListener('click', (ev) => this._onMouseClick(ev));
        this._canvas.addEventListener('mouseout', (ev) => this._onMouseOut(ev));
        this.$view.appendChild(this._canvas);
        this._ctx = this._canvas.getContext('2d');
        this._redraw = true;
        this._centerTuning = true;          // center tuning / free tuning mode flag
        this._centerFrequency = 89100000;   // default center frequency
        this._bandwidth = 2400000;          // default bandwidth
        this._data = [];                    // input data array
        this._minLX = 0;                    // minimum label of X
        this._maxLX = 1;                    // maximum label of X
        this._minLY = -90;                  // mininum label of Y
        this._maxLY = 0;                    // maximum label of Y
        this._minRX = 0;                    // minimum range of X (index of input data array)
        this._maxRX = null;                 // maximum range of X (index of input data array)
        this._minRY = -90;                  // minimum range of Y (value of input data array)
        this._maxRY = 0;                    // maximum range of Y (value of input data array)
        this._fillStyle = '#0000ff80';      // graph fill style
        this._freqMark = {                  // Center frequency cursor
            enabled:    true,                   // cursor enabled state
            color:      '#ff0000ff',            // cursor line color
            width:      1,                      // cursor line width in pixels
            pos:        0.5,                    // current horizontal position from 0.0. to 1.0
        };
        this._vertCur = {                   // Vertical cursor config:
            enabled:    true,                   // cursor enabled state
            visible:    false,                  // cursor visible state (inside graph)
            color:      '#ffffffCC',            // cursor line color
            width:      1,                      // cursor line width in pixels
            showFreq:   true,                   // show frequency value
            pos:        0.5,                    // current horizontal position from 0.0. to 1.0
        };
        this.$ready.push(this.render);
        this._log = logger.getLogger('App');
    },

    // Called when parent view is resized
    $setSize(width, height) {

        this._canvas.width = width;
        this._canvas.height = height;
        this._redraw = true;
    },

    // Render is called periodically by requestAnimationFrame to render this graph
    // The grid and the graph will be rendered only if necessary.
    render: function() {

        if (!this._redraw) {
            return;
        }
        this._drawGrid();
        this._drawGraph();
        this._redraw = false;
    },

    // Sets center tuning (true) of free tuning (false) mode.
    setCenterTuning(state) {

        this._centerTuning = state;
    },

    // Sets the center frequency of the graph in Hz.
    // Combined with the bandwith determines the x scale range
    setCenterFrequency: function(cf) {

        this._centerFrequency = cf;
        const minX = this._centerFrequency - this._bandwidth / 2;
        const maxX = this._centerFrequency + this._bandwidth / 2;
        this._setScaleX(minX, maxX);
    },

    // Sets the bandwidth of the graph in Hz.
    // Combined with the center frequency determines the x scale range
    setBandwidth: function(bw) {

        this._bandwidth = bw;
        const minX = this._centerFrequency - this._bandwidth / 2;
        const maxX = this._centerFrequency + this._bandwidth / 2;
        this._setScaleX(minX, maxX);
    },

    // Sets the range of X values which correspondes to the lower
    // and higher indices of the input data array.
    setRangeX: function(minRX, maxRX) {

        if (this._minRX !== minRX) {
            this._minRX = minRX;
            this._redraw = true;
        }
        if (this._maxRX !== maxRX) {
            this._maxRX = maxRX;
            this._redraw = true;
        }
    },

    // Sets the range of Y values
    setRangeY: function(minRY, maxRY) {

        if (this._minRY !== minRY) {
            this._minRY = minRY;
            this._redraw = true;
        }
        if (this._maxRY !== maxRY) {
            this._maxRY = maxRY;
            this._redraw = true;
        }
    },

    // Sets the X scale minimum and maximum label values
    _setScaleX: function(minLX, maxLX) {

        if (this._minLX !== minLX) {
            this._minLX = minLX;
            this._redraw = true;
        }
        if (this._maxLX !== maxLX) {
            this._maxLX = maxLX;
            this._redraw = true;
        }
    },

    // Sets the Y scale minimum and maximum label values
    setScaleY: function(minLY, maxLY) {

        if (this._minLY !== minLY) {
            this._minLY = minLY;
            this._redraw = true;
        }
        if (this._maxLY !== maxLY) {
            this._maxLY = maxLY;
            this._redraw = true;
        }
    },

    // Sets the data to graph
    setData: function(data) {
        
        this._data = data;
        this._redraw = true;
    },

    // Sets the fill style which could be null, a color, a canvas gradient
    // or a canvas pattern.
    setFillStyle: function(fillStyle) {

        this._fillStyle = fillStyle;
        this._redraw = true;
    },

    // Sets the vertical cursor horizontal position from -0.5 to 0.5.
    setVertCurPos(pos) {

        if (pos < -0.5) {
            pos = -0.5;
        }
        if (pos > 0.5) {
            pos = 0.5;
        }
        this._vertCur.pos = pos;
        if (this._vertCur.enabled && this._vertCur.visible) {
            this._redraw = true;
        }
    },

    // Formats frequency value (x scale)
    _formatFreq(value) {
        let suffix = '';
        if (value / 1e9 > 1) {
            value /= 1e9;
            suffix = 'G';
        } else
        if (value / 1e6 > 1) {
            value /= 1e6;
            suffix = 'M';
        } else
        if (value / 1e3 > 1) {
            value /= 1e3;
            suffix = 'K';
        }
        return value.toFixed(2) + suffix;
    },

    // Draws the grid and the X and Y scales
    _drawGrid: function() {

        // Redraw background
        const ctx = this._ctx;
        ctx.save();
        ctx.fillStyle = this.config.backgroundColor;
        ctx.fillRect(0, 0, this._canvas.width, this._canvas.height);

        ctx.lineWidth = 1;
        ctx.fillStyle = this.config.labelColor;
        ctx.strokeStyle = this.config.axisColor;
        ctx.font = this.config.labelFont;
        ctx.textBaseline = 'middle';
        ctx.textAlign = 'right';

        // Draw x axis and horizontal grid lines from top to bottom
        var cx = this.config.marginLeft - this.config.tickY;
        var cy = this.config.marginTop;
        const spaceVert = (this._canvas.height - this.config.marginTop - this.config.marginBottom) /
            (this.config.gridX + 1);
        var labY = this._maxLY;
        const deltaY = (this._maxLY - this._minLY) / (this.config.gridX + 1);
        while (cy <= this._canvas.height - this.config.marginBottom + 1) {
            const cyi = Math.floor(cy);
            ctx.fillText(labY.toFixed(1), cx, cyi);
            ctx.beginPath();
            ctx.moveTo(cx, cyi);
            ctx.lineTo(this._canvas.width - this.config.marginRight, cyi);
            ctx.stroke();
            cy += spaceVert;
            labY -= deltaY;
        }
        
        // Draw y axis and vertical grid lines from left to right
        ctx.textBaseline = 'top';
        ctx.textAlign = 'center';
        cx = this.config.marginLeft;
        cy = this._canvas.height - this.config.marginBottom + this.config.tickX;
        const spaceHoriz = (this._canvas.width - this.config.marginLeft - this.config.marginRight) /
            (this.config.gridY + 1);
        var labX = this._minLX;
        const deltaX = (this._maxLX - this._minLX) / (this.config.gridY + 1);
        while (cx <= this._canvas.width - this.config.marginRight + 1) {
            const cxi = Math.floor(cx);
            ctx.fillText(this._formatFreq(labX), cxi, cy);
            ctx.beginPath();
            ctx.moveTo(cxi, cy);
            ctx.lineTo(cxi, this.config.marginTop);
            ctx.stroke();
            cx += spaceHoriz;
            labX += deltaX;
        }

        const width = this._canvas.width - this.config.marginRight - this.config.marginLeft;
        const height = this._canvas.height - this.config.marginTop - this.config.marginBottom;

        // Draw the center frequency vertical cursor
        if (this._freqMark.enabled) {
            const cx = (this._freqMark.pos) * width;
            ctx.strokeStyle = this._freqMark.color;
            ctx.lineWidth = this._freqMark.width;
            ctx.beginPath();
            ctx.moveTo(this.config.marginLeft + cx, this.config.marginTop);
            ctx.lineTo(this.config.marginLeft + cx, this.config.marginTop + height);
            ctx.stroke();
        }

        // Draw the vertical cursor
        if (this._vertCur.enabled && this._vertCur.visible) {
            const cx = (this._vertCur.pos + 0.5) * width;
            ctx.strokeStyle = this._vertCur.color;
            ctx.lineWidth = this._vertCur.width;
            ctx.beginPath();
            const posX = this.config.marginLeft + cx;
            ctx.moveTo(posX, this.config.marginTop);
            ctx.lineTo(posX, this.config.marginTop + height);
            ctx.stroke();
            const freq = this._centerFrequency + this._vertCur.pos * this._bandwidth;
            ctx.textAlign = 'left';
            ctx.fillText(this._formatFreq(freq), posX + 4, this.config.marginTop + 10);
        }
        ctx.restore();
    },

    // Draws the data graph
    _drawGraph: function() {
     
        if (this._data.length == 0) {
            return;
        }
        const width = this._canvas.width - this.config.marginRight - this.config.marginLeft;
        const height = this._canvas.height - this.config.marginTop - this.config.marginBottom;
        //const data = this._convData(this._data, width);
        const data = this._data;
        const ctx = this._ctx;
        ctx.save();

        // Sets the clip path to the internal graph area
        ctx.beginPath();
        ctx.rect(this.config.marginLeft, this.config.marginTop, width, height);
        ctx.clip(); 

        // Starts the line graph path
        ctx.beginPath();

        // Sets graph line attributes
        ctx.lineWidth = this.config.graphLineWidth;
        ctx.strokeStyle = this.config.graphLineColor;

        var maxRX = this.maxRX === null ? data.length-1 : this.maxRX;
        maxRX = maxRX < data.length ? maxRX : data.length - 1;
        const minRX = this._minRX;
        const minRY = this._minRY;
        const marginLeft = this.config.marginLeft;
        const marginBottom = this.config.marginBottom;
        const rangeX = maxRX - minRX;
        const rangeY = this._maxRY - this._minRY;
        const canvasHeight = this._canvas.height;

        let start = true;
        if (this._fillStyle !== null) {
            ctx.fillStyle = this._fillStyle;
            ctx.moveTo(marginLeft-1, this.config.marginTop + height + 1);
            const cy = canvasHeight - (marginBottom + (data[0] - minRY) / rangeY * height);
            ctx.moveTo(marginLeft-1, cy);
            start = false;
        }

        let cy;
        for (var cx = 0; cx < width; cx += 1) {
            let idx =  Math.floor(cx/width * rangeX);
            cy = canvasHeight - (marginBottom + (data[idx] - minRY) / rangeY * height);
            if (start) {
                ctx.moveTo(cx + marginLeft, cy);
                start = false;
            } else {
                ctx.lineTo(cx + marginLeft, cy);
            }
        }
        
        if (this._fillStyle !== null) {
            ctx.lineTo(marginLeft + width + 1, cy);
            ctx.lineTo(marginLeft + width + 1, this.config.marginTop + height + 1);
            ctx.lineTo(marginLeft-1, this.config.marginTop + height + 1);
            ctx.fill();
        }
        ctx.stroke();
        ctx.restore();
    },

    // Returns the closest frequency using the frequency step for the frequency range.
    _cursorFreq(f) {

        // Find the step for the frequency range
        let range;
        for (range of cursorStepMap.keys()) {
            if (f < range) {
                break;
            }
        }
        let step = cursorStepMap.get(range);
        // Returns the closest integer frequency multiple of the step
        return Math.round(f/step) * step;
    },

    // Converts input fft psd 
    _convData: function(data, width) {
        
        let step = Math.floor(data.length / width);
        //console.log('Step', step);
        if (step <= 2) {
            return data;
        }
        const buf = new Float32Array(width);
        let j = 0;
        let max = -1000000;
        for (var i = 1; i < data.length; i++) {
            if (data[i] > max) {
                max = data[i];
            }
            //console.log("i, j, val, step, val/step", i, j, i%step, val, step, val/step);
            if ((i % step) == 0) {
                buf[j] = max;
                j++;
                max = -1000000;
            }
        }
        return buf;
    },

    // Returns mouse position relative to the canvas for the specified event
    _mousePos: function(ev) { 
       
        const rect = this._canvas.getBoundingClientRect();
        return {
            x: ev.clientX - rect.left,
            y: ev.clientY - rect.top
        };
    },

    // Called when mouse moves over the canvas
    _onMouseMove: function(ev) {

        let pos = this._mousePos(ev);
        //console.log('mouse', pos, this.config.marginLeft);
        const width = this._canvas.width - this.config.marginRight - this.config.marginLeft;
        const height = this._canvas.height - this.config.marginTop - this.config.marginBottom;
        if (this._vertCur.enabled) {
            if (pos.x < this.config.marginLeft || pos.x > this.config.marginLeft + width) {
                this._vertCur.visible = false;
                return;
            }
            if (pos.y < this.config.marginTop || pos.y > this.config.marginTop + height) {
                this._vertCur.visible = false;
                return;
            }
            this._vertCur.visible = true;
            let px = (pos.x - this.config.marginLeft) / width - 0.5;
            this.setVertCurPos(px);
        }
    },

    // Called when mouse moves out of the canvas
    _onMouseOut: function(/* ev */) {
        this._vertCur.visible = false;
        this._redraw = true;
    },

    // Called when mouse clicks over the canvas
    _onMouseClick: function(/* ev */) {

        // Frequency cursor
        if (this._vertCur.enabled && this._vertCur.visible) {
            let freq = this._centerFrequency + this._vertCur.pos * this._bandwidth;
            this.callEvent('onFrequency', [this._cursorFreq(freq)]); 
        }
    },

}, webix.ui.view, webix.EventSystem);



