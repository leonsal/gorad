'use strict';

// Prefix for all window ids
const winPrefix = 'rtlsdr.win';


// Prefix for all form ids
const formPrefix = 'rtlsdr.form';

// Current setup configuration
var config = {
    radioid:        '0',
    samplerate:     2400000,
    freqcorrection: 0,
    enableagc:      true,
};

createFormSetup();

//
// Creates form
//
function createFormSetup() {

    webix.ui({
        view:       'window',
        id:         `${winPrefix}.setup`,
        modal:      true,
        move:       true,
        position:   'center',
        head:       'RTL-SDR',
        width:      400,
        body: {
            view:   'form',
            id:     `${formPrefix}.setup`,
            scroll: false,
            elements: [
                {view: 'counter',
                    id:         `${formPrefix}.radioid`,
                    label:      'Radio index:',
                    name:       'radioid',
                },
                {view: 'richselect',
                    id:         `${formPrefix}.samplerate`,
                    label:      'Sample rate (Hz):',
                    name:       'samplerate',
                    value:      'all',
                    options:    [
                        {id:3200000, value: '3,200,000'},
                        {id:3000000, value: '3,000,000'},
                        {id:2800000, value: '2,800,000'},
                        {id:2400000, value: '2,400,000'},
                        {id:1920000, value: '1,920,000'},
                        {id:1800000, value: '1,800,000'},
                    ],
                },
                {view: 'counter',
                    id:         `${formPrefix}.freqcorrection`,
                    label:      'Frequency correction (PPM):',
                    name:       'freqcorrection',
                },
                {view: 'checkbox',
                    id:         `${formPrefix}.enableagc`,
                    label:      'Enable AGC:',
                    name:       'enableagc',
                },
                {margin: 5, cols: [
                    {view: 'button',
                        value:      'OK',
                        type:       'form',
                        click:      () => $$(`${formPrefix}.setup`).app.submit(),
                    },
                    {view: 'button',
                        value:      'Cancel',
                        type:       'form',
                        click:      () => $$(`${winPrefix}.setup`).hide(),
                    },
                ]},
            ],
            elementsConfig:{
                labelAlign: 'right',
                labelWidth: 200,
            },
        }
    });

    // Sets form methods
    $$(`${formPrefix}.setup`).app = {

        // Show setup form
        show: function () {
            $$(`${formPrefix}.setup`).setValues(config);
            $$(`${winPrefix}.setup`).show();
        },
       
        // Submit setup form
        submit: function() {

            // Get and saves configuration
            var values = $$(`${formPrefix}.setup`).getValues();
            config = values;
            config.radioid = config.radioid.toString();
            console.log(values);
            // Hides the setup window
            $$(`${winPrefix}.setup`).hide();
        },

        // Returns radio setup configuration
        getConfig: function() {
            return config;
        }
    };
}

