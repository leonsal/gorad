'use strict';

import './slider_wheel.js';
import './tuner.js';
import './fftgraph.js';
import './waterfall.js';
import * as icons from './icons';


function toolbar(app) {
    // Builds supported radios select options
    const radiosList = [];
    for (var radio in app._server.radios) {
        radiosList.push({id: radio, value: app._server.radios[radio]});
    }
    return {
        view: 'toolbar',
        height: 54,
        elements: [
            { view: 'button',
                type:  'icon',
                id:    'btnMenu',
                icon:  icons.Menu,
                width: 48
            },
            { view: 'label',
                label: `GoRad ${app.version}/${app._server.version}`,
                width: 132,
            },
            { view:'select',
                id:      'selRadio',
                label:   '',
                width:   200,
                options: radiosList,
            },
            {view: 'icon',
                id:      'btnConfig',
                icon:    icons.Settings,
                tooltip: 'Radio configuration',
                width:   48
            },
            { view: 'icon',
                id:      'btnStartStop',
                icon:    icons.Play,
                tooltip: 'Start/stop radio',
                width:   48
            },
            { view: 'icon',
                id:      'btnMute',
                icon:    icons.VolumeHigh,
                tooltip: 'Mute audio',
                width:   48
            },
            { view: 'slider_wheel',
                type:    'alt',
                id:      'sliderVol',
                label:   '', 
                value:   80,
                width:   180,
                title:   webix.template('Vol: #value#%'),
                tooltip: 'Audio volume',
            },
            { view: 'icon',
                id:      'btnTuningType',
                icon:    icons.CenterTuning,
                tooltip: 'Center tuning / Free tuning',
                width:   48
            },
            { view:      'tuner',
                id:      'tuner1',
                digits:  10,
                value:   89000000,
                width:   300,
                tooltip: 'Radio tuner frequency',
            },
        ]
    };
}

const leftpane = {
    view: 'scrollview',
    width: 200,
    body: {
        view: 'accordion',
        multi: true,
        scroll: 'y',
        rows: [
            { header: 'radio'},
            { header: 'radio2'},
            { header: 'radio3'},
            { header: 'radio4'},
            { header: 'radio5'},
            { header: 'radio6'},
            { header: 'radio6'},
            { header: 'radio6'},
            { header: 'radio6'},
            { header: 'radio6'},
            { header: 'radio6'},
            { header: 'radio6'},
            { header: 'radio8'},
            { header: 'radio9'},
        ],
    },
};


// Builds main screen
export function buildScreen(app) {

    webix.ui({
        view: 'layout',
        id:   'mainLayout',
        rows:[
            toolbar(app),
            { cols: [
                leftpane,
                { view: 'resizer' },
                { view: 'layout',
                    rows: [
                        { view: 'fftgraph', id: 'fftgraph1' },
                        { view: 'resizer' },
                        { view: 'waterfall', id: 'waterfall1' },
                    ]
                }
            ]},
        ],
    });
}

