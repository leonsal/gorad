'use strict';

webix.protoUI({

    name: 'tuner',

    // Properties default values
    defaults:{
        digits: 4,      // Number of digits
        value:  0,      // Initial value
        unit:   ' Hz',  // Unit name
        sep:    ',',    // Group separator
        max:    null,   // Maximum value
        min:    null,   // Minimum value
        width:  200,
    },          

    // Component initialization
    $init: function(config){

        this.$view.className = 'app_tuner';
        this._digits = config.digits || this.defaults.digits;
        this._value = config.value || this.defaults.value;
        const unit = config.unit || this.defaults.unit;
        const sep = config.sep || this.defaults.sep;
        if (config.max !== undefined) {
            this._max = config.max;
        } else  {
            this._max = Math.pow(10, this._digits) - 1;
        }
        this._elements = [];

        // Creates digits and separators
        for (let i = 0; i < this._digits; i++) {
            // Creates digit element
            const element = document.createElement('span');
            element.tabIndex = i + 1;
            // Sets event handlers
            const index = this._elements.length;
            element.addEventListener('wheel', (ev) => this._onDigitWheel(index, ev));
            element.addEventListener('keydown', (ev) => this._onDigitKeypress(index, ev));
            // Adds digit to view and to internal array
            this.$view.appendChild(element);
            this._elements.push(element);
            // Appends group separator if necessary
            let rest = this._digits - 1 - i;
            if ((rest > 0) && (rest % 3) == 0) {
                const el = document.createElement('span');
                el.innerHTML = sep;
                this.$view.appendChild(el);
            }
        }
        // Adds unit name 
        const element = document.createElement('span');
        element.innerHTML = unit;
        this.$view.appendChild(element);
        // Sets initial value
        this.setValue(this._value);
    },

    // Sets value of the tuner frequency and generates event
    setValue: function(value, event=true) {
        this._value = value;
        for (let i = 0; i < this._digits; i++) {
            const pten = Math.pow(10, this._digits - i - 1);
            var dv = Math.floor(value / pten);
            value = value % pten;
            this._elements[i].innerHTML = dv;
        }
        if (event) {
            this.callEvent('onChange', [this._value]);
        }
    },

    // Returns the value of the frequency tuner
    value: function() {
        return this._value;
    },

    // Decrements digit value
    _decDigit(index) {
        const pten = this._digits - index - 1;
        const value = this._value - Math.pow(10, pten);
        if (value <= 0) {
            return;
        }
        this.setValue(value);
    },

    // Increments digit value
    _incDigit: function(index) {
        const pten = this._digits - index - 1;
        const value = this._value + Math.pow(10, pten);
        if (value >= this._max) {
            return;
        }
        this.setValue(value);
    },

    // Sets key focus to next digit if possible
    _nextDigit: function(index) {
        if (index >= this._digits - 1) {
            return;
        }
        index++;
        this._elements[index].focus();
    },

    // Sets key focus to previous digit if possible
    _prevDigit: function(index) {
        if (index <= 0) {
            return;
        }
        index--;
        this._elements[index].focus();
    },

    // Sets the value of the specified digit
    _setDigit(index, vset) {
        const digits = [];
        var value = this._value;
        for (let i = 0; i < this._digits; i++) {
            const pten = Math.pow(10, this._digits - i - 1);
            var dv = Math.floor(value / pten);
            value = value % pten;
            digits.push(dv);
        }
        digits[index] = vset;
        value = 0;
        for (let i = 0; i < digits.length; i++) {
            const pten = Math.pow(10, digits.length - i - 1);
            value += digits[i] * pten;
        }
        this.setValue(value);
    },

    // Called when digit receives wheel event
    _onDigitWheel: function(index, ev) {
        if (ev.deltaY > 0) {
            this._incDigit(index);
        } else {
            this._decDigit(index);
        }
    },

    // Called when digit receives key events
    _onDigitKeypress: function(index, ev) {
        if (ev.key >= '0' && ev.key <= '9') {
            this._setDigit(index, Number(ev.key));
            this._nextDigit(index);
            return;
        }
        switch (ev.key) {
        case 'ArrowUp':
            this._incDigit(index);
            break;
        case 'ArrowDown':
            this._decDigit(index);
            break;
        case 'ArrowRight':
            this._nextDigit(index);
            break;
        case 'ArrowLeft':
            this._prevDigit(index);
            break;
        case 'Backspace':
            this._setDigit(index, 0);
            this._prevDigit(index);
            break;
        }
    },

}, webix.ui.view,           // Base component
   webix.EventSystem);      // To support event handling capability


