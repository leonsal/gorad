'use strict';

import * as logger from './logger';
import * as icons from './icons';
import {buildScreen} from './main_screen';
import {Channel} from './channel';
import './rtlsdr';


//
// Application class
//
class App {


    constructor() {
        this.version = '1.2.3';
        this._cmdSock = new Channel();
        this._fftSock = new Channel();
        //this.evbus = new EventBus();
        this._graphs = [];
        this._audioMute = false;
        this._centerTuning = true;
        this._server = null;
        this._started = false;
        this._renderGraphs = this._renderGraphs.bind(this);
        this._log = logger.getLogger('App');
        this._centerFrequency = 0;
        this._filterFrequency = 0;
        this._bandwidth = 0;
        this._textDecoder = new TextDecoder('utf-8');
    }

    // Start rendering the graphs by requestAnimationFrame
    _renderGraphs() {
        for (var i = 0; i < this._graphs.length; i++) {
            this._graphs[i].render();
        }
        window.requestAnimationFrame(this._renderGraphs);
    }

    // Try to connect to server command socket.
    // Then connect to server FFT socket.
    connect() {
        this._cmdSock.connect('/ws', (err) => {
            if (err)  {
                webix.alert({
                    title:      'ERROR',
                    text:       'Error connection with server Command WebSocket',
                    ok:         'Retry',
                    callback:   () => this.connect(),
                });
                return;
            }
            this.connectFFT();
        });
    }

    // Try to connect to server FFT socket.
    // Then get configuration.
    connectFFT() {
        this._fftSock.connect('/fft', (err) => {
            if (err)  {
                webix.alert({
                    title:      'ERROR',
                    text:       'Error connection with server FFT WebSocket',
                    ok:         'Retry',
                    callback:   () => this.connectFFT(),
                });
                return;
            }
            // Sets callback when receiving binary data from this socket
            this._fftSock.onBinary((data) => this.onRxFFT(data));
            // Continue with get configuration
            this.getConfig();
        });
    }

    // Sends request to get server configurations
    getConfig() {
        this._cmdSock.send('getconfig', null, (resp) => {
            this._server = resp.data;
            this.setup();
        });
    }

    setup() {
        // Build screen
        buildScreen(this);

        // Sets selected radio
        $$('selRadio').setValue(this._server.radiodef);

        // Attach event handlers to ui controls
        $$('btnConfig').attachEvent('onItemClick', () => this.configRadio());
        $$('btnStartStop').attachEvent('onItemClick', () => this.onStartStop());
        //$$('btnStop').attachEvent('onItemClick', () => this.stop());
        $$('btnMute').attachEvent('onItemClick', () => this.audioMute());
        $$('sliderVol').attachEvent('onChange', () => this.audioVolume());
        $$('tuner1').attachEvent('onChange', (f) => this.onTunerControl(f));
        $$('btnTuningType').attachEvent('onItemClick', () => this.onTuningType());

        // Sets enabled states
        $$('selRadio').enable();
        $$('btnConfig').enable();
        $$('btnStartStop').enable();
        //$$('btnStop').disable();
        $$('tuner1').disable();

        // Attach event handler to fftgraph
        $$('fftgraph1').attachEvent('onFrequency', (f) => this.onFrequencyCursor(f));

        // Sets graphs to render
        this._graphs.push($$('fftgraph1'));
        this._graphs.push($$('waterfall1'));
        this._renderGraphs();
    }

    // Show radio configuration window
    configRadio() {
        const radioType = $$('selRadio').getValue();
        const formid = radioType + '.form.setup';
        this._log.debug('config radio ==========> %s', formid);
        $$(formid).app.show();
    }

    // Starts the selected radio
    start() {

        const radioType = $$('selRadio').getValue();
        const formid = radioType + '.form.setup';
        var config = $$(formid).app.getConfig();
        config.radiotype = radioType;
        this._cmdSock.send('start', config, (resp) => {
            if (resp.err) {
                webix.alert({
                    title:  'ERROR',
                    text:   resp.err,
                    ok:     'OK',
                });
                $$('btnStartStop').enable();
                return;
            }

            // Sets initial frequency
            let frequency = this._centerFrequency == 0 ? 89100000 : this._centerFrequency;
            $$('tuner1').setValue(frequency);

            // Update controls
            $$('selRadio').disable();
            $$('btnConfig').disable();
            $$('btnStartStop').enable();
            $$('btnStartStop').define('icon', icons.Stop);
            $$('btnStartStop').refresh();
            this._started = true;
        });
    }

    // Stops the radio
    stop() {

        // Sends command to server
        this._cmdSock.send('stop', null, (resp) => {
            if (resp.err) {
                webix.alert({
                    title:  'ERROR',
                    text:   resp.err,
                    ok:     'OK',
                });
            }
        });

        // Update controls
        $$('selRadio').enable();
        $$('btnConfig').enable();
        $$('tuner1').disable();
        $$('btnStartStop').enable();
        $$('btnStartStop').define('icon', icons.Play);
        $$('btnStartStop').refresh();
        this._started = false;
    }

    // Toggle radio start / stop
    onStartStop() {

        $$('btnStartStop').disable();
        if (!this._started) {
            this.start();
        } else {
            this.stop();
        }
    }

    // Toggle audio mute
    audioMute() {

        this._audioMute = !this._audioMute;
        var icon = icons.VolumeMute;
        if (!this._audioMute) {
            icon = icons.VolumeHigh;
        }
        $$('btnMute').define('icon', icon);
        $$('btnMute').refresh();

        this._cmdSock.send('audiomute', {mute: this._audioMute}, (resp) => {
            this._log.debug('audiomute resp: %s', resp);    
            if (resp.err) {
                return;
            }
        });
    }

    // Set audio volume
    audioVolume() {

        const val = $$('sliderVol').getValue();
        this._cmdSock.send('audiovol', {vol: val / 100}, (resp) => {
            this._log.debug('audiovol resp: %s', resp);    
            if (resp.err) {
                return;
            }
        });
    }

    // Called when tuner control frequency changes
    onTunerControl(freq) {

        this.sendFrequency(freq, 0);
    }

    // Called when tuning type button is clicked
    onTuningType() {

        this._centerTuning = !this._centerTuning;
        var icon = icons.CenterTuning;
        if (!this._centerTuning) {
            icon = icons.FreeTuning;
        }
        $$('btnTuningType').define('icon', icon);
        $$('btnTuningType').refresh();
    }

    // Called when the frequency cursor in the FFT graph is clicked
    onFrequencyCursor(freq) {

        this.sendFrequency(freq, 0);
    }

    // Called with received FFT header and data (ArrayBuffer) from server
    onRxFFT(data) {

        const minDataSize = 512;
        // Checks if header was received
        if (data.byteLength < minDataSize) {
            // Decodes ArrayBuffer with bytes in UTF8 to string
            //let decoder = new TextDecoder('utf-8');
            let decoded = this._textDecoder.decode(data);
            // Parse decoded JSON string
            const header = JSON.parse(decoded);
            //console.log('header', header);
            // Updates center frequency if necessary
            if (this._centerFrequency != header.CenterFrequency) {
                this._centerFrequency = header.CenterFrequency;
                $$('tuner1').setValue(this._centerFrequency, false);
                $$('fftgraph1').setCenterFrequency(this._centerFrequency);
                console.log('updated center frequency:', this._centerFrequency);
            }
            // Updates filter frequency if necessary
            if (this._filterFrequency != header.FilterFrequency) {
                this._filterFrequency = header.FilterFrequency;
                console.log('updated filter frequency:', this._filterFrequency);
            }
            // Updates bandwidth if necessary
            if (this._bandwidth != header.Bandwidth) {
                this._bandwidth =  header.Bandwidth;
                $$('fftgraph1').setBandwidth(this._bandwidth);
                console.log('updated bandwidth:', this._bandwidth);
            }
        // Otherwise it is FFT data
        } else {
            const buf = new Float32Array(data);
            $$('fftgraph1').setData(buf);
            $$('waterfall1').setData(buf);
        }
    }

    // Sends command to set the receiver center and filter frequencies.
    sendFrequency(centerFreq, filterFreq) {

        let params = {
            CenterFreq: Math.floor(centerFreq),
            FilterFreq: filterFreq,
        };
        this._cmdSock.send('setfreq', params, (resp) => {
            this._log.debug('setfreq resp: %s', resp);
        });
    }
}

// Creates logger
const log = logger.getLogger('MAIN');
const hconsole = logger.createConsoleHandler({
    level:      logger.DEBUG,
    useColors:  true
});
log.addHandler(hconsole);
log.setLevel('DEBUG');
log.info('Starting...');

//
// Creates application instance and starts it
//
const app = new App();
app.connect();


