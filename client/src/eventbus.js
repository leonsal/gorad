"use strict";

export class EventBus {

    constructor() {
        this._events = {}
    }

    // Register listener for event
    listen(evname, cb, ctx = null) {

        var subs = this._events[evname];
        if (subs === undefined) {
            subs = [];
            this._events[evname] = subs;
        }
        subs.push({cb, ctx});
    }

    // Unregister listener for the specified event name and callback
    // Returns the number of listeners found and unregistered
    unlisten(evname, cb) {
        var found = 0
        const subs = this._events[evname];
        if (subs === undefined) {
            return found;
        }
        var i = 0
        while (i < subs.length) {
            if (subs[i].cb != cb) {
                i++;
                continue;
            }
            subs.splice(i, 1);
            found++;
        }
        return found;
    }

    // Emits event calling all registered listeners
    emit(evname, ...args) {
        const subs = this._events[evname];
        if (subs === undefined) {
            return;
        }
        for (var i = 0; i < subs.length; i++) {
            const sub = subs[i];
            if (sub.ctx !== null) {
                sub.call(sub.ctx, sub.cb(args));
            } else {
                sub.cb(args);
            }
        }
    }
}

