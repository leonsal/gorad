'use strict';

import {EventBus} from './eventbus';

// Websocket readyStates:
export const CONNECTING = 0;
export const OPEN = 1;
export const CLOSING = 2;
export const CLOSED = 3;

// Generated events
export const ON_OPENED = 'opened';
export const ON_CLOSED = 'closed';
export const ON_ERROR  = 'error';
export const ON_RXMSG  = 'rxmsg';


//
// WebSocket channel class
//
export class Channel extends EventBus {

    constructor() {
        super();
        this._socket = null;    // WebSocket object
        this._nextid = 1;       // ID for next request
        this._resp = {};        // Expected responses by id
        this._bincb = null;     // Callback for received binary messages
    }

    // Try to connect to the server using the specified relative url.
    // Calls the specified callback with the connection result
    connect(relurl, cb) {

        // Close current connection if necessary
        if (this._socket && this._socket.readyState != CLOSED) {
            this._socket.close();
            this._socket = null;
        }
        
        // Open web socket connection with server using the supplied relative URL
        const url = 'ws://' + document.location.host + relurl;
        this._socket = new WebSocket(url);
        this._socket.binaryType = 'arraybuffer';

        // Sets initial event handlers for connection callback
        this._socket.onerror = (ev) => cb('error');
        this._socket.onclose = (code, reason) => cb('closed');

        // When connection is opened sets normal event handlers
        this._socket.onopen = () => {
            this._socket.onerror = (ev) => this._onError(ev);
            this._socket.onclose = (code, reason) => this._onClose(code, reason);
            this.emit(ON_OPENED);
            cb('');   
        };
        this._socket.onmessage = (ev) => this._onMessage(ev);
    }

    close() {
        if (this._socket.readyState != CLOSED) {
            return;
        }
        this._socket.close();
    }

    // Returns the socket ready state
    status() {
        if (!this._socket) {
            return CLOSED;
        }
        return this._socket.readyState;
    }

    // Sends message with name and data
    // The optional callback will be called when a response for
    // this message is received.
    send(name, data, cb=null) {

        // Checks if connection is open
        if (this._socket.readyState != OPEN) {
            if (cb) {
                cb({err:'connection is not open'});
            }
            return;
        }

        // Builds message envelope
        const msg = {
            id:   this._nextid,
            name: name,
            data: JSON.stringify(data),
        };
        this._nextid++;

        // If callback supplied, waits response of this message
        if (cb) {
            this._resp[msg.id] = {cb};
        }

        // Encodes message to JSON and sends is as text
        const text = JSON.stringify(msg);
        this._socket.send(text);
    }

    // Sets callback to call when binary messages are received
    onBinary(cb=null) {

        this._bincb = cb;
    }

    // Called when a message is received from the socket.
    // The message can be a response to a previous sent request or a
    // message initiated by the server.
    _onMessage(ev) {

        // Messages of type string are JSON encoded
        if (typeof(ev.data) == 'string') {
            //console.log('_onMessage:', ev.data);
            const msg = JSON.parse(ev.data);
            // If message id is not present or id==0, emits an event with this message
            if (!msg.id) {
                this.emit(ON_RXMSG);
                return;
            }
            // Checks if this message is response from a previous request
            const resp = this._resp[msg.id];
            if (resp) {
                resp.cb(msg);
                delete this._resp[msg.id];
                return;
            }
            console.log('Received response without request');
            return;
        }

        // Messages of type binary (ArrayBuffer)
        if (this._bincb) {
            this._bincb(ev.data);
        }
    }

    _onClose(code, reason) {
        console.log('_onClose:', code, reason);
        this.emit(ON_CLOSED);
    }

    _onError(ev) {
        console.log('_onError:', this, ev);
        this.emit(ON_ERROR);
    }
}

