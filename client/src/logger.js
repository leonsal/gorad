/**
 * Logger
 * @module logger
 */
"use strict";
import {vsprintf} from "./sprintf";

// Log level numbers
export const DEBUG   = 0;
export const INFO    = 1;
export const WARN    = 2;
export const ERROR   = 3;
export const FATAL   = 4;

// Log level names
const LEVELS = {
    0:  "DEBUG",
    1:  "INFO ",
    2:  "WARN ",
    3:  "ERROR",
    4:  "FATAL"
};

// ROOT LOGGER
var root = null;

// Map with all loggers indexed by prefix
var loggers = {};
// Only for debugging, allowing enabling/disabling logs by the console
window.LOGGERS=loggers;


/**
 * Create root logger or return logger based on root logger
 * @param {string} prefix   Options prefix string
 * @return {object}         Logger object instance
 */
export function getLogger(prefix) {

    if (root === null) {
        root = new Logger(prefix);
        return root;
    }
    return createLogger(prefix, root);
}


/**
 * Creates logger
 * @param {string} prefix - Optional prefix string
 * @param {object} parent - Optional parent logger
 * @return {object}       - Logger object instance
 */
export function createLogger(prefix, parent) {

    return Object.seal(new Logger(prefix, parent));
}


/**
 * Internal logger constructor
 * @constructor
 */
class Logger {

    constructor(prefix, parent) {

        if (parent) {
            this._prefix = parent._prefix;
        } else {
            this._prefix = '';
        }
        if (prefix) {
            if (this._prefix.length > 0) {
                this._prefix += '/';
            }
            this._prefix += prefix;
        }
        this._level     = DEBUG;
        this._handlers  = [];
        this.enabled    = true;

        // Copy parent logger attributes
        if (parent !== undefined) {
            this._level = parent.level;
            for (var pos = 0; pos < parent._handlers.length; pos++) {
                this._handlers.push(parent._handlers[pos]);
            }
        }
        // Saves logger in global map
        loggers[prefix] = this;
    }


    /**
     * Set the logger minimum level
     * lname: String with level name
     */
    setLevel(lname) {
        var level;
        
        level = getLevelNo(lname);
        if (level === null) {
            throw "Invalid log level: " + level;
        }
        this._level = level;
    }


    /**
     * Add log handlers
     * handler: Log handler object
     */
    addHandler(handler) {

        if (handler._emit === undefined) {
            throw "Invalid handler";
        }
        this._handlers.push(handler);
    }


    /**
     * Logs message
     * level:   Message numeric level
     * format:  Format string
     */
    log(level, format) {

        // If specified level less than global minimum, ignore message
        if (!this.enabled || (level < this._level)) {
            return;
        }

        // Formats user message
        var args = [];
        for (var pos = 2; pos < arguments.length; pos++) {
            args.push(arguments[pos]);
        }
        var msg = vsprintf(format, args);

        // Builds log event
        var logev = {
            date:    new Date(),
            level:   level,
            prefix:  this._prefix,
            msg:     msg
        };

        // Call handlers
        for (pos = 0; pos < this._handlers.length; pos++) {
            var handler = this._handlers[pos];
            // Checks handler specific log level, if defined
            if (handler.level && (level < handler.level)) {
                continue;
            }
            handler._emit.call(handler, logev);
        }
        // If FATAL close logs and EXIT process
        if (level == FATAL) {
            this.close();
        }
    }


    /**
     * Close all log handlers
     */
    close() {

        for (var pos = 0; pos < this._handlers.length; pos++) {
            var handler = this._handlers[pos];
            handler._close.call(handler);
        }
    };


    /**
     * Emits DEBUG log message
     * format: Format string
     * ...:    Parameters
     */
    debug() {

        var args = [DEBUG];
        for (var pos = 0; pos < arguments.length; pos++) {
            args.push(arguments[pos]);
        }
        this.log.apply(this, args);
    };


    /**
     * Emits INFO log message
     * format: Format string
     * ...:    Parameters
     */
    info() {

        var args = [INFO];
        for (var pos = 0; pos < arguments.length; pos++) {
            args.push(arguments[pos]);
        }
        this.log.apply(this, args);
    };


    /**
     * Emits WARN log message
     * format: Format string
     * args:   Format arguments
     */
    warn() {

        var args = [WARN];
        for (var pos = 0; pos < arguments.length; pos++) {
            args.push(arguments[pos]);
        }
        this.log.apply(this, args);
    };


    /**
     * Emits ERROR log message
     * format: Format string
     * ...:    Parameters
     */
    error() {

        var args = [ERROR];
        for (var pos = 0; pos < arguments.length; pos++) {
            args.push(arguments[pos]);
        }
        this.log.apply(this, args);
    };


    /**
     * Emits FATAL log message
     * format: Format string
     * ...:    Parameters
     */
    fatal() {

        var args = [FATAL];
        for (var pos = 0; pos < arguments.length; pos++) {
            args.push(arguments[pos]);
        }
        this.log.apply(this, args);

    }
}

/**
 * Get level number from its name
 * lname:  Level name
 * return: Level number of null
 */
function getLevelNo(lname) {
    var value;

    lname = lname.toUpperCase();
    for (value in LEVELS) {
        if (LEVELS[value] == lname) {
            return value;
        }
    }
    return null;
}

/**
 * Format log event
 * handler: Handler object
 * logev:   Log event object
 * return:  String with formatted log event
 */
function formatEvent(handler, logev) {

    var mparts = [];
    // Formats timestamp
    var now = logev.date;
    if (handler.showDate) {
        mparts.push(String(now.getFullYear()));
        mparts.push('-');
        mparts.push(ljust(String(now.getMonth()+1), 2));
        mparts.push('-');
        mparts.push(ljust(String(now.getDate()), 2));
        mparts.push(' ');
    }
    if (handler.showTime) {
        mparts.push(ljust(String(now.getHours()), 2));
        mparts.push(':');
        mparts.push(ljust(String(now.getMinutes()), 2));
        mparts.push(':');
        mparts.push(ljust(String(now.getSeconds()), 2));
        mparts.push('.');
        mparts.push(ljust(String(now.getMilliseconds()), 3));
    }
    // Level
    mparts.push(' ' + LEVELS[logev.level]);
    // Prefix
    mparts.push(' ' + logev.prefix + ' ');
    // User message
    mparts.push(logev.msg);
    return mparts.join('');
}



/**
 * Left justify field
 * field:   String
 * len:     Field minimum length
 * pchar:   Option padding char (default='0')
 * return:  String left justifined with zeros
 */
function ljust(field, len, pchar) {
    var npads,
        pads;

    pchar = pchar || '0';
    npads = len - field.length;
    if (npads <= 0) {
        return field;
    }
    pads = [];
    while (npads--) {
        pads.push(pchar);
    }
    pads.push(field);
    return pads.join('');
}


//-----------------------------------------------------------------------------
// Console Handler
//-----------------------------------------------------------------------------


/**
 * Creates Console Handler
 * config: Configuration object
 * return: Console handler object
 */
export function createConsoleHandler(config) {

    return Object.seal(new ConsoleHandler(config));
}


/**
 * Internal console handler constructor
 * config: Configuration object
 */
class ConsoleHandler {

    constructor(config) {
        this._config = config;
        this._level  = null;
        this._colors  = {
            0:  'background: #FFFFFF; color: #000000;',
            1:  'background: #FFFFFF; color: #000000;',
            2:  'background: #FFFFFF; color: #0000FF;',
            3:  'background: #FFFFFF; color: #FF0000;',
        };
        this.showDate   = false;
        this.showTime   = true;

        // If level name was defined, get its number
        if (config.level) {
            this._level = getLevelNo(config.level);
            if (this._level === null) {
                throw "ConsoleHandler: Invalid log level";
            }
        }
    }


    /**
     * Emits console log
     * logev: Log event object
     */
    _emit(logev) {

        var msg = formatEvent(this, logev);
        if (this._config.useColors) {
            console.log('%c' + msg, this._colors[logev.level]);
        } else {
            console.log(msg);
        }
    }


    /**
     * Close console handler
     */
    _close() {
        
    };
}


//-----------------------------------------------------------------------------
// Generic handler
//-----------------------------------------------------------------------------

/**
 * Creates generic Handler
 * fwrite: Function to write to the log
 * fclose: Function to close the log
 */
export function createHandler(fwrite, fclose = null) {

    return Object.seal(new Handler(fwrite, fclose));

}

/**
 * Internal constructor
 */
class Handler {

    constructor(fwrite, fclose) {
        this._fwrite    = fwrite;
        this._fclose    = fclose;
        this.showDate   = false;
        this.showTime   = true;
    }

    /**
     * Emits log
     * logev: Log event object
     */
    _emit(logev) {

        var msg = formatEvent(this, logev);
        this._fwrite(msg + "\n");
    };

    /**
     * Close handler
     */
    _close() {

        if (this._fclose) {
            this._fclose();
        }
    };
}

