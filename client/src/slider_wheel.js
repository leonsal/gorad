'use strict';

webix.protoUI({

    name: "slider_wheel",

    $init: function(config) {
        this.$view.addEventListener("wheel", (ev) => this._onWheel(ev));
    },

    // Called when 'wheel' event received over the slider
    _onWheel: function(ev) {
        var value = Number(this.getValue());
        if (ev.deltaY > 0) {
            if (value < Number(this.config.max)) {
                value++;
            }
        } else {
            if (value > Number(this.config.min)) {
                value--;
            }
        }
        this.setValue(value)
    },
    
}, webix.ui.slider);

