'use strict';

export const Menu           = 'mdi mdi-menu';
export const Settings       = 'mdi mdi-settings';
export const Play           = 'mdi mdi-play';
export const Stop           = 'mdi mdi-stop';
export const VolumeMute     = 'mdi mdi-volume-mute';
export const VolumeHigh     = 'mdi mdi-volume-high';
export const CenterTuning   = 'mdi mdi-format-horizontal-align-center';
export const FreeTuning     = 'mdi mdi-repeat';


