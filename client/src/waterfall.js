/**
 * Webix UI Component to show FFT waterfall using a canvas element
 */
'use strict';

webix.protoUI({

    name: 'waterfall',

    // Default configuration values which can be overriden by the user configuration
    defaults: {
        marginTop:          0,                     // Top margin width in pixels
        marginBottom:       0,                     // Bottom margin width in pixels
        marginLeft:         64,                    // Left margin width in pixels
        marginRight:        30,                    // Right margin width in pixels
        backgroundColor:    '#000000',             // Background color
    },

    // Component initialization
    // config is an object which can override default configurations
    $init: function(/* config */) {

        // SETS the container view to relative positioning
        // Important for the overlay canvas to work with absolute positioning.
        this.$view.style.position = 'relative';

        // Creates main canvas for the waterfall graph
        this._cvGraph = document.createElement('canvas');
        this.$view.appendChild(this._cvGraph);
        this._ctxGraph = this._cvGraph.getContext('2d');

        // Create cursor canvas over the graph canvas and sets event listeners
        this._cvCursor = document.createElement('canvas');
        this._cvCursor.style.position = 'absolute';
        this._cvCursor.style.left = '0px';
        this._cvCursor.style.top = '0px';
        this._cvCursor.addEventListener('mousemove',  (ev) => this._onMouseMove(ev));
        this._cvCursor.addEventListener('click', (ev) => this._onMouseClick(ev));
        this._cvCursor.addEventListener('mouseout', (ev) => this._onMouseOut(ev));
        this.$view.appendChild(this._cvCursor);
        this._ctxCursor = this._cvCursor.getContext('2d');

        // Creates hidden auxiliary canvas
        this._cvHidden = document.createElement('canvas');
        this._ctxHidden = this._cvHidden.getContext('2d');

        this._redrawData = true;
        this._redrawCursor = true;
        this._data = [];                                // input data array
        this._centerFrequency = 89100000;               // default center frequency
        this._bandwidth = 2400000;                      // default bandwidth
        this._minRX = 0;                                // minimum range of X (index of input data array)
        this._maxRX = null;                             // maximum range of X (index of input data array)
        this._minRY = -90;                              // minimum range of Y (value of input data array)
        this._maxRY = 0;                                // maximum range of Y (value of input data array)
        this._colorScale = chroma.scale('Spectral');    // creates default color scale
        this._colorScale.domain([this._minRY, this._maxRY]);
        this._vertCur = {                               // Vertical cursor config:
            enabled:    true,                               // cursor enabled state
            visible:    false,                              // cursor visible state (inside graph)
            color:      '#000000ff',                        // cursor line color
            width:      1,                                  // cursor line width in pixels
            showFreq:   true,                               // show frequency value
            pos:        0.5,                                // current horizontal position from 0.0. to 1.0
        };
    },

    // Called when parent view is resized
    $setSize(width, height) {

        // Resize main canvas. It will be cleared.
        this._cvGraph.width = width;
        this._cvGraph.height = height;

        // Copy hidden auxiliary canvas to main canvas canvas scaling if necessary
        this._ctxGraph.drawImage(this._cvHidden, 0, 0, this._cvHidden.width, this._cvHidden.height, 0, 0, this._cvGraph.width, this._cvGraph.height);

        // Resize the cursor canvas. It will be cleared
        this._cvCursor.width = width;
        this._cvCursor.height = height;

        // Resize the hidden auxiliary canvas. It will be cleared.
        this._cvHidden.width = width;
        this._cvHidden.height = height;

        this._redrawData = true;
        this._redrawCursor = true;
    },

    // Render is called periodically by requestAnimationFrame to render the waterfall data
    // and the cursor if necessary
    render: function() {

        if (this._redrawData) {
            this._drawData();
            this._redrawData = false;
        }
        if (this._redrawCursor) {
            this._drawCursor();
            this._redrawCursor = false;
        }
    },

    // Sets the center frequency of the graph in Hz.
    setCenterFrequency: function(cf) {

        this._centerFrequency = cf;
    },

    // Sets the bandwidth of the graph in Hz.
    setBandwidth: function(bw) {

        this._bandwidth = bw;
    },

    // Sets the next data line to graph
    setData: function(data) {
        
        this._data = data;
        this._redrawData = true;
    },

    // Sets the range of Y values
    setRangeY: function(minRY, maxRY) {

        if (this._minRY !== minRY) {
            this._minRY = minRY;
            this._redrawData = true;
        }
        if (this._maxRY !== maxRY) {
            this._maxRY = maxRY;
            this._redrawData = true;
        }
    },

    // Sets the vertical cursor horizontal position from -0.5 to 0.5.
    setVertCurPos(pos) {

        if (pos < -0.5) {
            pos = -0.5;
        }
        if (pos > 0.5) {
            pos = 0.5;
        }
        this._vertCur.pos = pos;
        if (this._vertCur.enabled && this._vertCur.visible) {
            this._redrawCursor = true;
        }
    },

    // Draws the current data on the first line of the waterfall using the current color scale.
    _drawData: function() {

        if (this._data.length == 0) {
            return;
        }
        const width = this._cvGraph.width - this.config.marginRight - this.config.marginLeft;
        const data = this._data;

        // Copy current canvas to auxiliary hidden canvas
        this._ctxHidden.drawImage(this._cvGraph, 0, 0);

        // Redraw background 
        const ctx = this._ctxGraph;
        ctx.save();
        ctx.fillStyle = this.config.backgroundColor;
        ctx.fillRect(0, 0, this._cvGraph.width, this._cvGraph.height);

        const maxRX = this._maxRX === null ? data.length-1 : this._maxRX;
        const rangeX = maxRX - this._minRX;
        const marginLeft = this.config.marginLeft;
        const marginTop  = this.config.marginTop;

        // Generates line with image data from FFT data
        // and draw this line at the top of the graph.
        const imgLine = ctx.createImageData(this._cvGraph.width, 1);
        const imgData = imgLine.data;
        for (var cx = 0; cx < width; cx++) {
            const idxData =  Math.floor(cx/width * rangeX);
            const color = this._colorScale(data[idxData]).rgb();
            const idx = cx * 4;
            imgData[idx]     = color[0];
            imgData[idx + 1] = color[1];
            imgData[idx + 2] = color[2];
            imgData[idx + 3] = 255;
        }
        ctx.putImageData(imgLine, marginLeft, marginTop);

        // Copy the auxiliary canvas over the main canvas one line down
        ctx.drawImage(this._cvHidden, 0, 0, this._cvHidden.width, this._cvHidden.height, 0, 1, this._cvGraph.width, this._cvGraph.height);
        ctx.restore();
    },

    // Draws the cursor on the cursor canvas which is overlaid over the graph canvas
    _drawCursor: function() {

        // Clears the canvas to transparent
        const ctx = this._ctxCursor;
        ctx.clearRect(0, 0, this._cvCursor.width, this._cvCursor.height);

        if (!this._vertCur.visible || !this._vertCur.enabled) {
            return;
        }

        // Draws the vertical cursor line
        const width = this._cvCursor.width - this.config.marginRight - this.config.marginLeft;
        const height = this._cvCursor.height - this.config.marginTop - this.config.marginBottom;
        const cx = (this._vertCur.pos + 0.5) * width;
        ctx.strokeStyle = this._vertCur.color;
        ctx.lineWidth = this._vertCur.width;
        ctx.beginPath();
        const posX = this.config.marginLeft + cx;
        ctx.moveTo(posX, this.config.marginTop);
        ctx.lineTo(posX, this.config.marginTop + height);
        ctx.stroke();

        // Draws frequency text
        //const freq = this._centerFrequency + this._vertCur.pos * this._bandwidth;
        //ctx.textAlign = 'left';
        //ctx.fillText(this._formatFreq(freq), posX + 4, this.config.marginTop + 10);
    },

    _draw2: function() {

        this._ctxCursor.fillStyle = '#ff000002';
        this._ctxCursor.fillRect(0, 0, this._cvCursor.width, this._cvCursor.height);

        if (this._data.length == 0) {
            return;
        }
        const width = this._canvas.width - this.config.marginRight - this.config.marginLeft;
        const data = this._data;

        // Copy auxiliary hidden canvas to itself one line down
        const ximage = this._ctxHidden.getImageData(0, 0, this._canvas.width, this._canvas.height-1);
        this._ctxHidden.putImageData(ximage, 0, 1);

        const maxRX = this._maxRX === null ? data.length-1 : this._maxRX;
        const rangeX = maxRX - this._minRX;
        const marginLeft = this.config.marginLeft;
        const marginTop  = this.config.marginTop;

        // Generates line with image data from FFT data
        // and draw this line at the top of the graph.
        const imgLine = this._ctxHidden.createImageData(this._cvHidden.width, 1);
        const imgData = imgLine.data;
        for (var cx = 0; cx < width; cx++) {
            const idxData =  Math.floor(cx/width * rangeX);
            const color = this._colorScale(data[idxData]).rgb();
            const idx = cx * 4;
            imgData[idx]     = color[0];
            imgData[idx + 1] = color[1];
            imgData[idx + 2] = color[2];
            imgData[idx + 3] = 255;
        }
        this._ctxHidden.putImageData(imgLine, marginLeft, marginTop);
        // Copy the auxiliary canvas over the main canvas
        this._ctx.drawImage(this._cvHidden, 0, 0);
    },


    // Returns mouse position relative to the canvas for the specified event
    _mousePos: function(ev, cv) { 
       
        const rect = cv.getBoundingClientRect();
        return {
            x: ev.clientX - rect.left,
            y: ev.clientY - rect.top
        };
    },

    // Called when the mouse cursor moves over the cursor canvas
    _onMouseMove: function(ev) {

        const pos = this._mousePos(ev, this._cvCursor);
        //console.log('mouse', pos, this.config.marginLeft);
        const width = this._cvCursor.width - this.config.marginRight - this.config.marginLeft;
        const height = this._cvCursor.height - this.config.marginTop - this.config.marginBottom;
        if (this._vertCur.enabled) {
            if (pos.x < this.config.marginLeft || pos.x > this.config.marginLeft + width) {
                this._vertCur.visible = false;
                return;
            }
            if (pos.y < this.config.marginTop || pos.y > this.config.marginTop + height) {
                this._vertCur.visible = false;
                return;
            }
            this._vertCur.visible = true;
            let px = (pos.x - this.config.marginLeft) / width - 0.5;
            this.setVertCurPos(px);
        }

    },

    // Called when the mouse is clicked over the cursor canvas
    _onMouseClick: function(/* ev */) {

    },

    // Called when the mouse exits from the cursor canvas
    _onMouseOut: function(/* ev */) {

        this._vertCur.visible = false;
        this._redrawCursor = true;
    }

}, webix.ui.view, webix.EventSystem);


