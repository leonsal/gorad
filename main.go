package main

import (
	"flag"
	"log"
	"os"
	"os/signal"
	"runtime/pprof"
	"runtime/trace"
)

// Command line options
var (
	oAddress    = flag.String("address", "localhost:1234", "Server address")
	oClipath    = flag.String("client path", "", "Client html application path")
	oCpuProfile = flag.String("cpuprofile", "", "Activate cpu profiling writing profile to the specified file")
	oExecTrace  = flag.String("exectrace", "", "Activate execution tracer writing data to the specified file")
)

func main() {

	log.SetFlags(log.Ltime | log.Lmicroseconds)
	flag.Parse()
	app := NewApp(&AppConfig{address: *oAddress, clipath: *oClipath})

	// Creates channel to receive OS signal interrupt notifications (Ctrl-C)
	ctrc := make(chan os.Signal, 2)
	signal.Notify(ctrc, os.Interrupt)

	// Start cpu profiling if requested
	if *oCpuProfile != "" {
		f, err := os.Create(*oCpuProfile)
		if err != nil {
			log.Printf("Error creating profile file:%s", err)
			return
		}
		defer f.Close()
		err = pprof.StartCPUProfile(f)
		if err != nil {
			log.Printf("Error starting CPU profile:%s", err)
			return
		}
		defer pprof.StopCPUProfile()
		log.Printf("Started writing CPU profile to: %s", *oCpuProfile)
	}

	// Start execution trace if requested
	if *oExecTrace != "" {
		f, err := os.Create(*oExecTrace)
		if err != nil {
			log.Printf("Error creating execution trace file:%s", err)
			return
		}
		defer f.Close()
		err = trace.Start(f)
		if err != nil {
			log.Printf("Error starting execution trace:%s", err)
		}
		defer trace.Stop()
		log.Printf("Started writing execution trace to: %s", *oExecTrace)
	}

	//log.Printf("Starting listen on: %v\n", *oAddress)
	//app.Start()

	// Starts application server in a goroutine
	stopped := make(chan bool)
	go func() {
		log.Printf("Starting listen on: %v\n", *oAddress)
		err := app.Start()
		log.Printf("%s\n", err)
		stopped <- true
	}()

	// Waits for OS interrupt
	<-ctrc
	app.Stop()

	// Waits for application goroutine to end
	<-stopped
}
