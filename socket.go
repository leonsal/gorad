package main

import (
	"encoding/json"
	"log"
	"net/http"
	"unsafe"

	"github.com/gorilla/websocket"
)

type Socket struct {
	conn        *websocket.Conn           // Websocket connection
	txbuffer    chan interface{}          // Transmission buffer channel
	msgHandlers map[string]MessageHandler // Maps message name to handler
}

// Message is the type for all messages envelopes sent by the client
type Message struct {
	Id   int    // Message ID
	Name string // Message name
	Data string // Optional message data in JSON
}

// Response is the type for all server responses
type Response struct {
	Id   int         `json:"id"`             // Same id as the request
	Err  string      `json:"err,omitempty"`  // Optional error message
	Data interface{} `json:"data,omitempty"` // Optional response data in JSON
}

// MessageHandler is the type for received message handlers
type MessageHandler func(s *Socket, m *Message)

// NewSocket creates and returns a pointer to a new Websocket connection
func NewSocket() *Socket {

	s := new(Socket)
	s.txbuffer = make(chan interface{})
	s.msgHandlers = make(map[string]MessageHandler)
	return s
}

// TxBuffer returns the transmission buffer channel
func (s *Socket) TxBuffer() chan interface{} {

	return s.txbuffer
}

// Register register a message handler.
// The specified handler will called when a message with the specified name is received
func (s *Socket) Register(mname string, handler MessageHandler) {

	s.msgHandlers[mname] = handler
}

// Sends response to previous received message with the specified id.
// The response include an optional error string and optional data
func (s *Socket) SendResponse(id int, errmsg string, data interface{}) {

	// Encodes the response
	resp := Response{Id: id, Err: errmsg, Data: data}
	encresp, err := json.Marshal(resp)
	if err != nil {
		log.Printf("Error encoding response:%v -> %v", resp, err)
	}

	// Inserts encoded response in transmission buffer
	s.txbuffer <- string(encresp)
}

// Close closes the socket
func (s *Socket) Close() {

	s.txbuffer <- struct{}{} // finish sender goroutine
	s.conn.Close()           // close socket will finish Handler goroutine
	s.conn = nil
}

// Handler runs in a goroutine started by the web server and
// process received messages
func (s *Socket) Handler(w http.ResponseWriter, r *http.Request) {

	// Upgrades HTTP connection to WebSocket connection
	var upgrader = websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
	}
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println("upgrade:", err)
		return
	}
	s.conn = conn
	log.Printf("websocket:%p connected\n", s)
	defer conn.Close()

	// Starts sender goroutine
	go s.sender()

	// Receive message loop
	for {
		mt, message, err := conn.ReadMessage()
		if err != nil {
			log.Println("socket error:", err)
			s.txbuffer <- struct{}{}
			break
		}
		s.decode(mt, message)
	}
}

// sender runs in a goroutine waiting for messages to be sent.
func (s *Socket) sender() {

	log.Printf("websocket sender:%p started\n", s)
	// Blocks reading from transmission buffer channel
	for msg := range s.txbuffer {
		var err error
		switch m := msg.(type) {
		// Send text message (JSON marshalled responses)
		case string:
			err = s.conn.WriteMessage(websocket.TextMessage, []byte(m))
		// Sends binary message with byte slice
		case []byte:
			err = s.conn.WriteMessage(websocket.BinaryMessage, m)
		// Sends binary message with float32 slice
		case []float32:
			size := len(m) * int(unsafe.Sizeof(float32(0)))
			var buf []byte
			buf = (*[1 << 30]byte)(unsafe.Pointer(&m[0]))[:size]
			err = s.conn.WriteMessage(websocket.BinaryMessage, buf)
		// Terminates goroutine
		case struct{}:
			log.Printf("websocket sender:%p stopped\n", s)
			return
		// Invalid message type
		default:
			log.Printf("Invalid message type to send")
		}
		// Checks for error writing message
		if err != nil {
			log.Println("Error sending message:", err)
		}
	}
}

// decode decodes received message and dispatch it to respective handler
func (s *Socket) decode(mtype int, mdata []byte) {

	log.Printf("recv: %s", mdata)
	// TextMessages are decoded using JSON
	// Only the message envelope is decoded.
	// The message data is decoded by its respective handler
	if mtype == websocket.TextMessage {
		var msg Message
		err := json.Unmarshal(mdata, &msg)
		if err != nil {
			log.Printf("error: %v decoding message", err)
			return
		}
		log.Printf("rx:%+v\n", &msg)

		// Get the message handler
		handler, ok := s.msgHandlers[msg.Name]
		if !ok {
			log.Printf("No message handler found for:%s", msg.Name)
			return
		}
		handler(s, &msg)
		return
	}
	log.Printf("Received message type: %v not supported", mtype)
}
