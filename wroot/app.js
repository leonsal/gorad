(function () {
	'use strict';

	var re = {
	    not_string: /[^s]/,
	    number: /[dief]/,
	    json: /[j]/,
	    not_json: /[^j]/,
	    text: /^[^\x25]+/,
	    modulo: /^\x25{2}/,
	    placeholder: /^\x25(?:([1-9]\d*)\$|\(([^\)]+)\))?(\+)?(0|'[^$])?(-)?(\d+)?(?:\.(\d+))?([b-fijosuxX])/,
	    key: /^([a-z_][a-z_\d]*)/i,
	    key_access: /^\.([a-z_][a-z_\d]*)/i,
	    index_access: /^\[(\d+)\]/,
	    sign: /^[\+\-]/
	};

	function sprintf() {
	    var key = arguments[0], cache = sprintf.cache;
	    if (!(cache[key] && cache.hasOwnProperty(key))) {
	        cache[key] = sprintf.parse(key);
	    }
	    return sprintf.format.call(null, cache[key], arguments)
	}

	sprintf.format = function(parse_tree, argv) {
	    var cursor = 1, tree_length = parse_tree.length, node_type = "", arg, output = [], i, k, match, pad, pad_character, pad_length, is_positive = true, sign = "";
	    for (i = 0; i < tree_length; i++) {
	        node_type = get_type(parse_tree[i]);
	        if (node_type === "string") {
	            output[output.length] = parse_tree[i];
	        }
	        else if (node_type === "array") {
	            match = parse_tree[i]; // convenience purposes only
	            if (match[2]) { // keyword argument
	                arg = argv[cursor];
	                for (k = 0; k < match[2].length; k++) {
	                    if (!arg.hasOwnProperty(match[2][k])) {
	                        throw new Error(sprintf("[sprintf] property '%s' does not exist", match[2][k]))
	                    }
	                    arg = arg[match[2][k]];
	                }
	            }
	            else if (match[1]) { // positional argument (explicit)
	                arg = argv[match[1]];
	            }
	            else { // positional argument (implicit)
	                arg = argv[cursor++];
	            }

	            if (get_type(arg) == "function") {
	                arg = arg();
	            }

	            if (re.not_string.test(match[8]) && re.not_json.test(match[8]) && (get_type(arg) != "number" && isNaN(arg))) {
	                throw new TypeError(sprintf("[sprintf] expecting number but found %s", get_type(arg)))
	            }

	            if (re.number.test(match[8])) {
	                is_positive = arg >= 0;
	            }

	            switch (match[8]) {
	                case "b":
	                    arg = arg.toString(2);
	                break
	                case "c":
	                    arg = String.fromCharCode(arg);
	                break
	                case "d":
	                case "i":
	                    arg = parseInt(arg, 10);
	                break
	                case "j":
	                    arg = JSON.stringify(arg, null, match[6] ? parseInt(match[6]) : 0);
	                break
	                case "e":
	                    arg = match[7] ? arg.toExponential(match[7]) : arg.toExponential();
	                break
	                case "f":
	                    arg = match[7] ? parseFloat(arg).toFixed(match[7]) : parseFloat(arg);
	                break
	                case "o":
	                    arg = arg.toString(8);
	                break
	                case "s":
	                    arg = ((arg = String(arg)) && match[7] ? arg.substring(0, match[7]) : arg);
	                break
	                case "u":
	                    arg = arg >>> 0;
	                break
	                case "x":
	                    arg = arg.toString(16);
	                break
	                case "X":
	                    arg = arg.toString(16).toUpperCase();
	                break
	            }
	            if (re.json.test(match[8])) {
	                output[output.length] = arg;
	            }
	            else {
	                if (re.number.test(match[8]) && (!is_positive || match[3])) {
	                    sign = is_positive ? "+" : "-";
	                    arg = arg.toString().replace(re.sign, "");
	                }
	                else {
	                    sign = "";
	                }
	                pad_character = match[4] ? match[4] === "0" ? "0" : match[4].charAt(1) : " ";
	                pad_length = match[6] - (sign + arg).length;
	                pad = match[6] ? (pad_length > 0 ? str_repeat(pad_character, pad_length) : "") : "";
	                output[output.length] = match[5] ? sign + arg + pad : (pad_character === "0" ? sign + pad + arg : pad + sign + arg);
	            }
	        }
	    }
	    return output.join("")
	};

	sprintf.cache = {};

	sprintf.parse = function(fmt) {
	    var _fmt = fmt, match = [], parse_tree = [], arg_names = 0;
	    while (_fmt) {
	        if ((match = re.text.exec(_fmt)) !== null) {
	            parse_tree[parse_tree.length] = match[0];
	        }
	        else if ((match = re.modulo.exec(_fmt)) !== null) {
	            parse_tree[parse_tree.length] = "%";
	        }
	        else if ((match = re.placeholder.exec(_fmt)) !== null) {
	            if (match[2]) {
	                arg_names |= 1;
	                var field_list = [], replacement_field = match[2], field_match = [];
	                if ((field_match = re.key.exec(replacement_field)) !== null) {
	                    field_list[field_list.length] = field_match[1];
	                    while ((replacement_field = replacement_field.substring(field_match[0].length)) !== "") {
	                        if ((field_match = re.key_access.exec(replacement_field)) !== null) {
	                            field_list[field_list.length] = field_match[1];
	                        }
	                        else if ((field_match = re.index_access.exec(replacement_field)) !== null) {
	                            field_list[field_list.length] = field_match[1];
	                        }
	                        else {
	                            throw new SyntaxError("[sprintf] failed to parse named argument key")
	                        }
	                    }
	                }
	                else {
	                    throw new SyntaxError("[sprintf] failed to parse named argument key")
	                }
	                match[2] = field_list;
	            }
	            else {
	                arg_names |= 2;
	            }
	            if (arg_names === 3) {
	                throw new Error("[sprintf] mixing positional and named placeholders is not (yet) supported")
	            }
	            parse_tree[parse_tree.length] = match;
	        }
	        else {
	            throw new SyntaxError("[sprintf] unexpected placeholder")
	        }
	        _fmt = _fmt.substring(match[0].length);
	    }
	    return parse_tree
	};

	function vsprintf(fmt, argv, _argv) {
	    _argv = (argv || []).slice(0);
	    _argv.splice(0, 0, fmt);
	    return sprintf.apply(null, _argv)
	}

	/**
	 * helpers
	 */
	function get_type(variable) {
	    return Object.prototype.toString.call(variable).slice(8, -1).toLowerCase()
	}

	function str_repeat(input, multiplier) {
	    return Array(multiplier + 1).join(input)
	}

	/**
	 * Logger
	 * @module logger
	 */

	// Log level numbers
	const DEBUG   = 0;
	const INFO    = 1;
	const WARN    = 2;
	const ERROR   = 3;
	const FATAL   = 4;

	// Log level names
	const LEVELS = {
	    0:  "DEBUG",
	    1:  "INFO ",
	    2:  "WARN ",
	    3:  "ERROR",
	    4:  "FATAL"
	};

	// ROOT LOGGER
	var root = null;

	// Map with all loggers indexed by prefix
	var loggers = {};
	// Only for debugging, allowing enabling/disabling logs by the console
	window.LOGGERS=loggers;


	/**
	 * Create root logger or return logger based on root logger
	 * @param {string} prefix   Options prefix string
	 * @return {object}         Logger object instance
	 */
	function getLogger(prefix) {

	    if (root === null) {
	        root = new Logger(prefix);
	        return root;
	    }
	    return createLogger(prefix, root);
	}


	/**
	 * Creates logger
	 * @param {string} prefix - Optional prefix string
	 * @param {object} parent - Optional parent logger
	 * @return {object}       - Logger object instance
	 */
	function createLogger(prefix, parent) {

	    return Object.seal(new Logger(prefix, parent));
	}


	/**
	 * Internal logger constructor
	 * @constructor
	 */
	class Logger {

	    constructor(prefix, parent) {

	        if (parent) {
	            this._prefix = parent._prefix;
	        } else {
	            this._prefix = '';
	        }
	        if (prefix) {
	            if (this._prefix.length > 0) {
	                this._prefix += '/';
	            }
	            this._prefix += prefix;
	        }
	        this._level     = DEBUG;
	        this._handlers  = [];
	        this.enabled    = true;

	        // Copy parent logger attributes
	        if (parent !== undefined) {
	            this._level = parent.level;
	            for (var pos = 0; pos < parent._handlers.length; pos++) {
	                this._handlers.push(parent._handlers[pos]);
	            }
	        }
	        // Saves logger in global map
	        loggers[prefix] = this;
	    }


	    /**
	     * Set the logger minimum level
	     * lname: String with level name
	     */
	    setLevel(lname) {
	        var level;
	        
	        level = getLevelNo(lname);
	        if (level === null) {
	            throw "Invalid log level: " + level;
	        }
	        this._level = level;
	    }


	    /**
	     * Add log handlers
	     * handler: Log handler object
	     */
	    addHandler(handler) {

	        if (handler._emit === undefined) {
	            throw "Invalid handler";
	        }
	        this._handlers.push(handler);
	    }


	    /**
	     * Logs message
	     * level:   Message numeric level
	     * format:  Format string
	     */
	    log(level, format) {

	        // If specified level less than global minimum, ignore message
	        if (!this.enabled || (level < this._level)) {
	            return;
	        }

	        // Formats user message
	        var args = [];
	        for (var pos = 2; pos < arguments.length; pos++) {
	            args.push(arguments[pos]);
	        }
	        var msg = vsprintf(format, args);

	        // Builds log event
	        var logev = {
	            date:    new Date(),
	            level:   level,
	            prefix:  this._prefix,
	            msg:     msg
	        };

	        // Call handlers
	        for (pos = 0; pos < this._handlers.length; pos++) {
	            var handler = this._handlers[pos];
	            // Checks handler specific log level, if defined
	            if (handler.level && (level < handler.level)) {
	                continue;
	            }
	            handler._emit.call(handler, logev);
	        }
	        // If FATAL close logs and EXIT process
	        if (level == FATAL) {
	            this.close();
	        }
	    }


	    /**
	     * Close all log handlers
	     */
	    close() {

	        for (var pos = 0; pos < this._handlers.length; pos++) {
	            var handler = this._handlers[pos];
	            handler._close.call(handler);
	        }
	    };


	    /**
	     * Emits DEBUG log message
	     * format: Format string
	     * ...:    Parameters
	     */
	    debug() {

	        var args = [DEBUG];
	        for (var pos = 0; pos < arguments.length; pos++) {
	            args.push(arguments[pos]);
	        }
	        this.log.apply(this, args);
	    };


	    /**
	     * Emits INFO log message
	     * format: Format string
	     * ...:    Parameters
	     */
	    info() {

	        var args = [INFO];
	        for (var pos = 0; pos < arguments.length; pos++) {
	            args.push(arguments[pos]);
	        }
	        this.log.apply(this, args);
	    };


	    /**
	     * Emits WARN log message
	     * format: Format string
	     * args:   Format arguments
	     */
	    warn() {

	        var args = [WARN];
	        for (var pos = 0; pos < arguments.length; pos++) {
	            args.push(arguments[pos]);
	        }
	        this.log.apply(this, args);
	    };


	    /**
	     * Emits ERROR log message
	     * format: Format string
	     * ...:    Parameters
	     */
	    error() {

	        var args = [ERROR];
	        for (var pos = 0; pos < arguments.length; pos++) {
	            args.push(arguments[pos]);
	        }
	        this.log.apply(this, args);
	    };


	    /**
	     * Emits FATAL log message
	     * format: Format string
	     * ...:    Parameters
	     */
	    fatal() {

	        var args = [FATAL];
	        for (var pos = 0; pos < arguments.length; pos++) {
	            args.push(arguments[pos]);
	        }
	        this.log.apply(this, args);

	    }
	}

	/**
	 * Get level number from its name
	 * lname:  Level name
	 * return: Level number of null
	 */
	function getLevelNo(lname) {
	    var value;

	    lname = lname.toUpperCase();
	    for (value in LEVELS) {
	        if (LEVELS[value] == lname) {
	            return value;
	        }
	    }
	    return null;
	}

	/**
	 * Format log event
	 * handler: Handler object
	 * logev:   Log event object
	 * return:  String with formatted log event
	 */
	function formatEvent(handler, logev) {

	    var mparts = [];
	    // Formats timestamp
	    var now = logev.date;
	    if (handler.showDate) {
	        mparts.push(String(now.getFullYear()));
	        mparts.push('-');
	        mparts.push(ljust(String(now.getMonth()+1), 2));
	        mparts.push('-');
	        mparts.push(ljust(String(now.getDate()), 2));
	        mparts.push(' ');
	    }
	    if (handler.showTime) {
	        mparts.push(ljust(String(now.getHours()), 2));
	        mparts.push(':');
	        mparts.push(ljust(String(now.getMinutes()), 2));
	        mparts.push(':');
	        mparts.push(ljust(String(now.getSeconds()), 2));
	        mparts.push('.');
	        mparts.push(ljust(String(now.getMilliseconds()), 3));
	    }
	    // Level
	    mparts.push(' ' + LEVELS[logev.level]);
	    // Prefix
	    mparts.push(' ' + logev.prefix + ' ');
	    // User message
	    mparts.push(logev.msg);
	    return mparts.join('');
	}



	/**
	 * Left justify field
	 * field:   String
	 * len:     Field minimum length
	 * pchar:   Option padding char (default='0')
	 * return:  String left justifined with zeros
	 */
	function ljust(field, len, pchar) {
	    var npads,
	        pads;

	    pchar = pchar || '0';
	    npads = len - field.length;
	    if (npads <= 0) {
	        return field;
	    }
	    pads = [];
	    while (npads--) {
	        pads.push(pchar);
	    }
	    pads.push(field);
	    return pads.join('');
	}


	//-----------------------------------------------------------------------------
	// Console Handler
	//-----------------------------------------------------------------------------


	/**
	 * Creates Console Handler
	 * config: Configuration object
	 * return: Console handler object
	 */
	function createConsoleHandler(config) {

	    return Object.seal(new ConsoleHandler(config));
	}


	/**
	 * Internal console handler constructor
	 * config: Configuration object
	 */
	class ConsoleHandler {

	    constructor(config) {
	        this._config = config;
	        this._level  = null;
	        this._colors  = {
	            0:  'background: #FFFFFF; color: #000000;',
	            1:  'background: #FFFFFF; color: #000000;',
	            2:  'background: #FFFFFF; color: #0000FF;',
	            3:  'background: #FFFFFF; color: #FF0000;',
	        };
	        this.showDate   = false;
	        this.showTime   = true;

	        // If level name was defined, get its number
	        if (config.level) {
	            this._level = getLevelNo(config.level);
	            if (this._level === null) {
	                throw "ConsoleHandler: Invalid log level";
	            }
	        }
	    }


	    /**
	     * Emits console log
	     * logev: Log event object
	     */
	    _emit(logev) {

	        var msg = formatEvent(this, logev);
	        if (this._config.useColors) {
	            console.log('%c' + msg, this._colors[logev.level]);
	        } else {
	            console.log(msg);
	        }
	    }


	    /**
	     * Close console handler
	     */
	    _close() {
	        
	    };
	}

	const Menu           = 'mdi mdi-menu';
	const Settings       = 'mdi mdi-settings';
	const Play           = 'mdi mdi-play';
	const Stop           = 'mdi mdi-stop';
	const VolumeMute     = 'mdi mdi-volume-mute';
	const VolumeHigh     = 'mdi mdi-volume-high';
	const CenterTuning   = 'mdi mdi-format-horizontal-align-center';
	const FreeTuning     = 'mdi mdi-repeat';

	webix.protoUI({

	    name: "slider_wheel",

	    $init: function(config) {
	        this.$view.addEventListener("wheel", (ev) => this._onWheel(ev));
	    },

	    // Called when 'wheel' event received over the slider
	    _onWheel: function(ev) {
	        var value = Number(this.getValue());
	        if (ev.deltaY > 0) {
	            if (value < Number(this.config.max)) {
	                value++;
	            }
	        } else {
	            if (value > Number(this.config.min)) {
	                value--;
	            }
	        }
	        this.setValue(value);
	    },
	    
	}, webix.ui.slider);

	webix.protoUI({

	    name: 'tuner',

	    // Properties default values
	    defaults:{
	        digits: 4,      // Number of digits
	        value:  0,      // Initial value
	        unit:   ' Hz',  // Unit name
	        sep:    ',',    // Group separator
	        max:    null,   // Maximum value
	        min:    null,   // Minimum value
	        width:  200,
	    },          

	    // Component initialization
	    $init: function(config){

	        this.$view.className = 'app_tuner';
	        this._digits = config.digits || this.defaults.digits;
	        this._value = config.value || this.defaults.value;
	        const unit = config.unit || this.defaults.unit;
	        const sep = config.sep || this.defaults.sep;
	        if (config.max !== undefined) {
	            this._max = config.max;
	        } else  {
	            this._max = Math.pow(10, this._digits) - 1;
	        }
	        this._elements = [];

	        // Creates digits and separators
	        for (let i = 0; i < this._digits; i++) {
	            // Creates digit element
	            const element = document.createElement('span');
	            element.tabIndex = i + 1;
	            // Sets event handlers
	            const index = this._elements.length;
	            element.addEventListener('wheel', (ev) => this._onDigitWheel(index, ev));
	            element.addEventListener('keydown', (ev) => this._onDigitKeypress(index, ev));
	            // Adds digit to view and to internal array
	            this.$view.appendChild(element);
	            this._elements.push(element);
	            // Appends group separator if necessary
	            let rest = this._digits - 1 - i;
	            if ((rest > 0) && (rest % 3) == 0) {
	                const el = document.createElement('span');
	                el.innerHTML = sep;
	                this.$view.appendChild(el);
	            }
	        }
	        // Adds unit name 
	        const element = document.createElement('span');
	        element.innerHTML = unit;
	        this.$view.appendChild(element);
	        // Sets initial value
	        this.setValue(this._value);
	    },

	    // Sets value of the tuner frequency and generates event
	    setValue: function(value, event=true) {
	        this._value = value;
	        for (let i = 0; i < this._digits; i++) {
	            const pten = Math.pow(10, this._digits - i - 1);
	            var dv = Math.floor(value / pten);
	            value = value % pten;
	            this._elements[i].innerHTML = dv;
	        }
	        if (event) {
	            this.callEvent('onChange', [this._value]);
	        }
	    },

	    // Returns the value of the frequency tuner
	    value: function() {
	        return this._value;
	    },

	    // Decrements digit value
	    _decDigit(index) {
	        const pten = this._digits - index - 1;
	        const value = this._value - Math.pow(10, pten);
	        if (value <= 0) {
	            return;
	        }
	        this.setValue(value);
	    },

	    // Increments digit value
	    _incDigit: function(index) {
	        const pten = this._digits - index - 1;
	        const value = this._value + Math.pow(10, pten);
	        if (value >= this._max) {
	            return;
	        }
	        this.setValue(value);
	    },

	    // Sets key focus to next digit if possible
	    _nextDigit: function(index) {
	        if (index >= this._digits - 1) {
	            return;
	        }
	        index++;
	        this._elements[index].focus();
	    },

	    // Sets key focus to previous digit if possible
	    _prevDigit: function(index) {
	        if (index <= 0) {
	            return;
	        }
	        index--;
	        this._elements[index].focus();
	    },

	    // Sets the value of the specified digit
	    _setDigit(index, vset) {
	        const digits = [];
	        var value = this._value;
	        for (let i = 0; i < this._digits; i++) {
	            const pten = Math.pow(10, this._digits - i - 1);
	            var dv = Math.floor(value / pten);
	            value = value % pten;
	            digits.push(dv);
	        }
	        digits[index] = vset;
	        value = 0;
	        for (let i = 0; i < digits.length; i++) {
	            const pten = Math.pow(10, digits.length - i - 1);
	            value += digits[i] * pten;
	        }
	        this.setValue(value);
	    },

	    // Called when digit receives wheel event
	    _onDigitWheel: function(index, ev) {
	        if (ev.deltaY > 0) {
	            this._incDigit(index);
	        } else {
	            this._decDigit(index);
	        }
	    },

	    // Called when digit receives key events
	    _onDigitKeypress: function(index, ev) {
	        if (ev.key >= '0' && ev.key <= '9') {
	            this._setDigit(index, Number(ev.key));
	            this._nextDigit(index);
	            return;
	        }
	        switch (ev.key) {
	        case 'ArrowUp':
	            this._incDigit(index);
	            break;
	        case 'ArrowDown':
	            this._decDigit(index);
	            break;
	        case 'ArrowRight':
	            this._nextDigit(index);
	            break;
	        case 'ArrowLeft':
	            this._prevDigit(index);
	            break;
	        case 'Backspace':
	            this._setDigit(index, 0);
	            this._prevDigit(index);
	            break;
	        }
	    },

	}, webix.ui.view,           // Base component
	   webix.EventSystem);      // To support event handling capability

	/**
	 * Webix UI Component to show FFT graph using a canvas element
	 */

	// Maps frequency range to frequency cursor step
	const cursorStepMap = new Map([
	    [ 1e6,  1e3],       // 0      to 1Mhz    -> 1Khz
	    [10e6,  10e3],      // 1Mhz   to 10Mhz   -> 10Khz
	    [80e6,  50e3],      // 10Mhz  to 80Mhz   -> 50Khz
	    [110e6, 100e3],     // 80Mhz  to 110Mhz  -> 100Khz
	    [200e6, 1e6],       // 200Mhz to 500Mhz  -> 1.000Mhz
	    [10006, 2e6],       // 500Mhz to 1Ghz    -> 2.000Mhz
	    [20006, 5e6],       // 2Gz    to ...     -> 5.000Mhz
	]);

	webix.protoUI({

	    name: 'fftgraph',

	    // Default configuration values which can be overriden by the user configuration
	    defaults: {
	        marginTop:          20,                    // Grid top margin width in pixels
	        marginBottom:       40,                    // Grid bottom margin width in pixels
	        marginLeft:         64,                    // Grid left margin width in pixels
	        marginRight:        30,                    // Grid right margin width in pixels
	        tickX:              16,                    // Length in pixels of x axis ticks
	        tickY:              16,                    // Length in pixels of y axis ticks
	        gridX:              8,                     // Number of x grid lines
	        gridY:              13,                    // Number of y grid lines
	        backgroundColor:    '#101010',             // Background color
	        axisColor:          '#505050',             // Axis and grid lines color
	        labelFont:          '10pt Helvetica',      // Font for axis labels
	        labelColor:         'white',               // Color for axis labels
	        graphLineWidth:     1,                     // Width for graph lines
	        graphLineColor:     '#FFFFFF',             // Color for graph lines
	    },

	    // Component initialization
	    // config is an object which can override default configurations
	    $init: function(/* config */) {

	        this.$view.className = 'app_fftgraph';
	        // Creates canvas element and appends to this view
	        this._canvas = document.createElement('canvas');
	        this._canvas.addEventListener('mousemove',  (ev) => this._onMouseMove(ev));
	        this._canvas.addEventListener('click', (ev) => this._onMouseClick(ev));
	        this._canvas.addEventListener('mouseout', (ev) => this._onMouseOut(ev));
	        this.$view.appendChild(this._canvas);
	        this._ctx = this._canvas.getContext('2d');
	        this._redraw = true;
	        this._centerTuning = true;          // center tuning / free tuning mode flag
	        this._centerFrequency = 89100000;   // default center frequency
	        this._bandwidth = 2400000;          // default bandwidth
	        this._data = [];                    // input data array
	        this._minLX = 0;                    // minimum label of X
	        this._maxLX = 1;                    // maximum label of X
	        this._minLY = -90;                  // mininum label of Y
	        this._maxLY = 0;                    // maximum label of Y
	        this._minRX = 0;                    // minimum range of X (index of input data array)
	        this._maxRX = null;                 // maximum range of X (index of input data array)
	        this._minRY = -90;                  // minimum range of Y (value of input data array)
	        this._maxRY = 0;                    // maximum range of Y (value of input data array)
	        this._fillStyle = '#0000ff80';      // graph fill style
	        this._freqMark = {                  // Center frequency cursor
	            enabled:    true,                   // cursor enabled state
	            color:      '#ff0000ff',            // cursor line color
	            width:      1,                      // cursor line width in pixels
	            pos:        0.5,                    // current horizontal position from 0.0. to 1.0
	        };
	        this._vertCur = {                   // Vertical cursor config:
	            enabled:    true,                   // cursor enabled state
	            visible:    false,                  // cursor visible state (inside graph)
	            color:      '#ffffffCC',            // cursor line color
	            width:      1,                      // cursor line width in pixels
	            showFreq:   true,                   // show frequency value
	            pos:        0.5,                    // current horizontal position from 0.0. to 1.0
	        };
	        this.$ready.push(this.render);
	        this._log = getLogger('App');
	    },

	    // Called when parent view is resized
	    $setSize(width, height) {

	        this._canvas.width = width;
	        this._canvas.height = height;
	        this._redraw = true;
	    },

	    // Render is called periodically by requestAnimationFrame to render this graph
	    // The grid and the graph will be rendered only if necessary.
	    render: function() {

	        if (!this._redraw) {
	            return;
	        }
	        this._drawGrid();
	        this._drawGraph();
	        this._redraw = false;
	    },

	    // Sets center tuning (true) of free tuning (false) mode.
	    setCenterTuning(state) {

	        this._centerTuning = state;
	    },

	    // Sets the center frequency of the graph in Hz.
	    // Combined with the bandwith determines the x scale range
	    setCenterFrequency: function(cf) {

	        this._centerFrequency = cf;
	        const minX = this._centerFrequency - this._bandwidth / 2;
	        const maxX = this._centerFrequency + this._bandwidth / 2;
	        this._setScaleX(minX, maxX);
	    },

	    // Sets the bandwidth of the graph in Hz.
	    // Combined with the center frequency determines the x scale range
	    setBandwidth: function(bw) {

	        this._bandwidth = bw;
	        const minX = this._centerFrequency - this._bandwidth / 2;
	        const maxX = this._centerFrequency + this._bandwidth / 2;
	        this._setScaleX(minX, maxX);
	    },

	    // Sets the range of X values which correspondes to the lower
	    // and higher indices of the input data array.
	    setRangeX: function(minRX, maxRX) {

	        if (this._minRX !== minRX) {
	            this._minRX = minRX;
	            this._redraw = true;
	        }
	        if (this._maxRX !== maxRX) {
	            this._maxRX = maxRX;
	            this._redraw = true;
	        }
	    },

	    // Sets the range of Y values
	    setRangeY: function(minRY, maxRY) {

	        if (this._minRY !== minRY) {
	            this._minRY = minRY;
	            this._redraw = true;
	        }
	        if (this._maxRY !== maxRY) {
	            this._maxRY = maxRY;
	            this._redraw = true;
	        }
	    },

	    // Sets the X scale minimum and maximum label values
	    _setScaleX: function(minLX, maxLX) {

	        if (this._minLX !== minLX) {
	            this._minLX = minLX;
	            this._redraw = true;
	        }
	        if (this._maxLX !== maxLX) {
	            this._maxLX = maxLX;
	            this._redraw = true;
	        }
	    },

	    // Sets the Y scale minimum and maximum label values
	    setScaleY: function(minLY, maxLY) {

	        if (this._minLY !== minLY) {
	            this._minLY = minLY;
	            this._redraw = true;
	        }
	        if (this._maxLY !== maxLY) {
	            this._maxLY = maxLY;
	            this._redraw = true;
	        }
	    },

	    // Sets the data to graph
	    setData: function(data) {
	        
	        this._data = data;
	        this._redraw = true;
	    },

	    // Sets the fill style which could be null, a color, a canvas gradient
	    // or a canvas pattern.
	    setFillStyle: function(fillStyle) {

	        this._fillStyle = fillStyle;
	        this._redraw = true;
	    },

	    // Sets the vertical cursor horizontal position from -0.5 to 0.5.
	    setVertCurPos(pos) {

	        if (pos < -0.5) {
	            pos = -0.5;
	        }
	        if (pos > 0.5) {
	            pos = 0.5;
	        }
	        this._vertCur.pos = pos;
	        if (this._vertCur.enabled && this._vertCur.visible) {
	            this._redraw = true;
	        }
	    },

	    // Formats frequency value (x scale)
	    _formatFreq(value) {
	        let suffix = '';
	        if (value / 1e9 > 1) {
	            value /= 1e9;
	            suffix = 'G';
	        } else
	        if (value / 1e6 > 1) {
	            value /= 1e6;
	            suffix = 'M';
	        } else
	        if (value / 1e3 > 1) {
	            value /= 1e3;
	            suffix = 'K';
	        }
	        return value.toFixed(2) + suffix;
	    },

	    // Draws the grid and the X and Y scales
	    _drawGrid: function() {

	        // Redraw background
	        const ctx = this._ctx;
	        ctx.save();
	        ctx.fillStyle = this.config.backgroundColor;
	        ctx.fillRect(0, 0, this._canvas.width, this._canvas.height);

	        ctx.lineWidth = 1;
	        ctx.fillStyle = this.config.labelColor;
	        ctx.strokeStyle = this.config.axisColor;
	        ctx.font = this.config.labelFont;
	        ctx.textBaseline = 'middle';
	        ctx.textAlign = 'right';

	        // Draw x axis and horizontal grid lines from top to bottom
	        var cx = this.config.marginLeft - this.config.tickY;
	        var cy = this.config.marginTop;
	        const spaceVert = (this._canvas.height - this.config.marginTop - this.config.marginBottom) /
	            (this.config.gridX + 1);
	        var labY = this._maxLY;
	        const deltaY = (this._maxLY - this._minLY) / (this.config.gridX + 1);
	        while (cy <= this._canvas.height - this.config.marginBottom + 1) {
	            const cyi = Math.floor(cy);
	            ctx.fillText(labY.toFixed(1), cx, cyi);
	            ctx.beginPath();
	            ctx.moveTo(cx, cyi);
	            ctx.lineTo(this._canvas.width - this.config.marginRight, cyi);
	            ctx.stroke();
	            cy += spaceVert;
	            labY -= deltaY;
	        }
	        
	        // Draw y axis and vertical grid lines from left to right
	        ctx.textBaseline = 'top';
	        ctx.textAlign = 'center';
	        cx = this.config.marginLeft;
	        cy = this._canvas.height - this.config.marginBottom + this.config.tickX;
	        const spaceHoriz = (this._canvas.width - this.config.marginLeft - this.config.marginRight) /
	            (this.config.gridY + 1);
	        var labX = this._minLX;
	        const deltaX = (this._maxLX - this._minLX) / (this.config.gridY + 1);
	        while (cx <= this._canvas.width - this.config.marginRight + 1) {
	            const cxi = Math.floor(cx);
	            ctx.fillText(this._formatFreq(labX), cxi, cy);
	            ctx.beginPath();
	            ctx.moveTo(cxi, cy);
	            ctx.lineTo(cxi, this.config.marginTop);
	            ctx.stroke();
	            cx += spaceHoriz;
	            labX += deltaX;
	        }

	        const width = this._canvas.width - this.config.marginRight - this.config.marginLeft;
	        const height = this._canvas.height - this.config.marginTop - this.config.marginBottom;

	        // Draw the center frequency vertical cursor
	        if (this._freqMark.enabled) {
	            const cx = (this._freqMark.pos) * width;
	            ctx.strokeStyle = this._freqMark.color;
	            ctx.lineWidth = this._freqMark.width;
	            ctx.beginPath();
	            ctx.moveTo(this.config.marginLeft + cx, this.config.marginTop);
	            ctx.lineTo(this.config.marginLeft + cx, this.config.marginTop + height);
	            ctx.stroke();
	        }

	        // Draw the vertical cursor
	        if (this._vertCur.enabled && this._vertCur.visible) {
	            const cx = (this._vertCur.pos + 0.5) * width;
	            ctx.strokeStyle = this._vertCur.color;
	            ctx.lineWidth = this._vertCur.width;
	            ctx.beginPath();
	            const posX = this.config.marginLeft + cx;
	            ctx.moveTo(posX, this.config.marginTop);
	            ctx.lineTo(posX, this.config.marginTop + height);
	            ctx.stroke();
	            const freq = this._centerFrequency + this._vertCur.pos * this._bandwidth;
	            ctx.textAlign = 'left';
	            ctx.fillText(this._formatFreq(freq), posX + 4, this.config.marginTop + 10);
	        }
	        ctx.restore();
	    },

	    // Draws the data graph
	    _drawGraph: function() {
	     
	        if (this._data.length == 0) {
	            return;
	        }
	        const width = this._canvas.width - this.config.marginRight - this.config.marginLeft;
	        const height = this._canvas.height - this.config.marginTop - this.config.marginBottom;
	        //const data = this._convData(this._data, width);
	        const data = this._data;
	        const ctx = this._ctx;
	        ctx.save();

	        // Sets the clip path to the internal graph area
	        ctx.beginPath();
	        ctx.rect(this.config.marginLeft, this.config.marginTop, width, height);
	        ctx.clip(); 

	        // Starts the line graph path
	        ctx.beginPath();

	        // Sets graph line attributes
	        ctx.lineWidth = this.config.graphLineWidth;
	        ctx.strokeStyle = this.config.graphLineColor;

	        var maxRX = this.maxRX === null ? data.length-1 : this.maxRX;
	        maxRX = maxRX < data.length ? maxRX : data.length - 1;
	        const minRX = this._minRX;
	        const minRY = this._minRY;
	        const marginLeft = this.config.marginLeft;
	        const marginBottom = this.config.marginBottom;
	        const rangeX = maxRX - minRX;
	        const rangeY = this._maxRY - this._minRY;
	        const canvasHeight = this._canvas.height;

	        let start = true;
	        if (this._fillStyle !== null) {
	            ctx.fillStyle = this._fillStyle;
	            ctx.moveTo(marginLeft-1, this.config.marginTop + height + 1);
	            const cy = canvasHeight - (marginBottom + (data[0] - minRY) / rangeY * height);
	            ctx.moveTo(marginLeft-1, cy);
	            start = false;
	        }

	        let cy;
	        for (var cx = 0; cx < width; cx += 1) {
	            let idx =  Math.floor(cx/width * rangeX);
	            cy = canvasHeight - (marginBottom + (data[idx] - minRY) / rangeY * height);
	            if (start) {
	                ctx.moveTo(cx + marginLeft, cy);
	                start = false;
	            } else {
	                ctx.lineTo(cx + marginLeft, cy);
	            }
	        }
	        
	        if (this._fillStyle !== null) {
	            ctx.lineTo(marginLeft + width + 1, cy);
	            ctx.lineTo(marginLeft + width + 1, this.config.marginTop + height + 1);
	            ctx.lineTo(marginLeft-1, this.config.marginTop + height + 1);
	            ctx.fill();
	        }
	        ctx.stroke();
	        ctx.restore();
	    },

	    // Returns the closest frequency using the frequency step for the frequency range.
	    _cursorFreq(f) {

	        // Find the step for the frequency range
	        let range;
	        for (range of cursorStepMap.keys()) {
	            if (f < range) {
	                break;
	            }
	        }
	        let step = cursorStepMap.get(range);
	        // Returns the closest integer frequency multiple of the step
	        return Math.round(f/step) * step;
	    },

	    // Converts input fft psd 
	    _convData: function(data, width) {
	        
	        let step = Math.floor(data.length / width);
	        //console.log('Step', step);
	        if (step <= 2) {
	            return data;
	        }
	        const buf = new Float32Array(width);
	        let j = 0;
	        let max = -1000000;
	        for (var i = 1; i < data.length; i++) {
	            if (data[i] > max) {
	                max = data[i];
	            }
	            //console.log("i, j, val, step, val/step", i, j, i%step, val, step, val/step);
	            if ((i % step) == 0) {
	                buf[j] = max;
	                j++;
	                max = -1000000;
	            }
	        }
	        return buf;
	    },

	    // Returns mouse position relative to the canvas for the specified event
	    _mousePos: function(ev) { 
	       
	        const rect = this._canvas.getBoundingClientRect();
	        return {
	            x: ev.clientX - rect.left,
	            y: ev.clientY - rect.top
	        };
	    },

	    // Called when mouse moves over the canvas
	    _onMouseMove: function(ev) {

	        let pos = this._mousePos(ev);
	        //console.log('mouse', pos, this.config.marginLeft);
	        const width = this._canvas.width - this.config.marginRight - this.config.marginLeft;
	        const height = this._canvas.height - this.config.marginTop - this.config.marginBottom;
	        if (this._vertCur.enabled) {
	            if (pos.x < this.config.marginLeft || pos.x > this.config.marginLeft + width) {
	                this._vertCur.visible = false;
	                return;
	            }
	            if (pos.y < this.config.marginTop || pos.y > this.config.marginTop + height) {
	                this._vertCur.visible = false;
	                return;
	            }
	            this._vertCur.visible = true;
	            let px = (pos.x - this.config.marginLeft) / width - 0.5;
	            this.setVertCurPos(px);
	        }
	    },

	    // Called when mouse moves out of the canvas
	    _onMouseOut: function(/* ev */) {
	        this._vertCur.visible = false;
	        this._redraw = true;
	    },

	    // Called when mouse clicks over the canvas
	    _onMouseClick: function(/* ev */) {

	        // Frequency cursor
	        if (this._vertCur.enabled && this._vertCur.visible) {
	            let freq = this._centerFrequency + this._vertCur.pos * this._bandwidth;
	            this.callEvent('onFrequency', [this._cursorFreq(freq)]); 
	        }
	    },

	}, webix.ui.view, webix.EventSystem);

	/**
	 * Webix UI Component to show FFT waterfall using a canvas element
	 */

	webix.protoUI({

	    name: 'waterfall',

	    // Default configuration values which can be overriden by the user configuration
	    defaults: {
	        marginTop:          0,                     // Top margin width in pixels
	        marginBottom:       0,                     // Bottom margin width in pixels
	        marginLeft:         64,                    // Left margin width in pixels
	        marginRight:        30,                    // Right margin width in pixels
	        backgroundColor:    '#000000',             // Background color
	    },

	    // Component initialization
	    // config is an object which can override default configurations
	    $init: function(/* config */) {

	        // SETS the container view to relative positioning
	        // Important for the overlay canvas to work with absolute positioning.
	        this.$view.style.position = 'relative';

	        // Creates main canvas for the waterfall graph
	        this._cvGraph = document.createElement('canvas');
	        this.$view.appendChild(this._cvGraph);
	        this._ctxGraph = this._cvGraph.getContext('2d');

	        // Create cursor canvas over the graph canvas and sets event listeners
	        this._cvCursor = document.createElement('canvas');
	        this._cvCursor.style.position = 'absolute';
	        this._cvCursor.style.left = '0px';
	        this._cvCursor.style.top = '0px';
	        this._cvCursor.addEventListener('mousemove',  (ev) => this._onMouseMove(ev));
	        this._cvCursor.addEventListener('click', (ev) => this._onMouseClick(ev));
	        this._cvCursor.addEventListener('mouseout', (ev) => this._onMouseOut(ev));
	        this.$view.appendChild(this._cvCursor);
	        this._ctxCursor = this._cvCursor.getContext('2d');

	        // Creates hidden auxiliary canvas
	        this._cvHidden = document.createElement('canvas');
	        this._ctxHidden = this._cvHidden.getContext('2d');

	        this._redrawData = true;
	        this._redrawCursor = true;
	        this._data = [];                                // input data array
	        this._centerFrequency = 89100000;               // default center frequency
	        this._bandwidth = 2400000;                      // default bandwidth
	        this._minRX = 0;                                // minimum range of X (index of input data array)
	        this._maxRX = null;                             // maximum range of X (index of input data array)
	        this._minRY = -90;                              // minimum range of Y (value of input data array)
	        this._maxRY = 0;                                // maximum range of Y (value of input data array)
	        this._colorScale = chroma.scale('Spectral');    // creates default color scale
	        this._colorScale.domain([this._minRY, this._maxRY]);
	        this._vertCur = {                               // Vertical cursor config:
	            enabled:    true,                               // cursor enabled state
	            visible:    false,                              // cursor visible state (inside graph)
	            color:      '#000000ff',                        // cursor line color
	            width:      1,                                  // cursor line width in pixels
	            showFreq:   true,                               // show frequency value
	            pos:        0.5,                                // current horizontal position from 0.0. to 1.0
	        };
	    },

	    // Called when parent view is resized
	    $setSize(width, height) {

	        // Resize main canvas. It will be cleared.
	        this._cvGraph.width = width;
	        this._cvGraph.height = height;

	        // Copy hidden auxiliary canvas to main canvas canvas scaling if necessary
	        this._ctxGraph.drawImage(this._cvHidden, 0, 0, this._cvHidden.width, this._cvHidden.height, 0, 0, this._cvGraph.width, this._cvGraph.height);

	        // Resize the cursor canvas. It will be cleared
	        this._cvCursor.width = width;
	        this._cvCursor.height = height;

	        // Resize the hidden auxiliary canvas. It will be cleared.
	        this._cvHidden.width = width;
	        this._cvHidden.height = height;

	        this._redrawData = true;
	        this._redrawCursor = true;
	    },

	    // Render is called periodically by requestAnimationFrame to render the waterfall data
	    // and the cursor if necessary
	    render: function() {

	        if (this._redrawData) {
	            this._drawData();
	            this._redrawData = false;
	        }
	        if (this._redrawCursor) {
	            this._drawCursor();
	            this._redrawCursor = false;
	        }
	    },

	    // Sets the center frequency of the graph in Hz.
	    setCenterFrequency: function(cf) {

	        this._centerFrequency = cf;
	    },

	    // Sets the bandwidth of the graph in Hz.
	    setBandwidth: function(bw) {

	        this._bandwidth = bw;
	    },

	    // Sets the next data line to graph
	    setData: function(data) {
	        
	        this._data = data;
	        this._redrawData = true;
	    },

	    // Sets the range of Y values
	    setRangeY: function(minRY, maxRY) {

	        if (this._minRY !== minRY) {
	            this._minRY = minRY;
	            this._redrawData = true;
	        }
	        if (this._maxRY !== maxRY) {
	            this._maxRY = maxRY;
	            this._redrawData = true;
	        }
	    },

	    // Sets the vertical cursor horizontal position from -0.5 to 0.5.
	    setVertCurPos(pos) {

	        if (pos < -0.5) {
	            pos = -0.5;
	        }
	        if (pos > 0.5) {
	            pos = 0.5;
	        }
	        this._vertCur.pos = pos;
	        if (this._vertCur.enabled && this._vertCur.visible) {
	            this._redrawCursor = true;
	        }
	    },

	    // Draws the current data on the first line of the waterfall using the current color scale.
	    _drawData: function() {

	        if (this._data.length == 0) {
	            return;
	        }
	        const width = this._cvGraph.width - this.config.marginRight - this.config.marginLeft;
	        const data = this._data;

	        // Copy current canvas to auxiliary hidden canvas
	        this._ctxHidden.drawImage(this._cvGraph, 0, 0);

	        // Redraw background 
	        const ctx = this._ctxGraph;
	        ctx.save();
	        ctx.fillStyle = this.config.backgroundColor;
	        ctx.fillRect(0, 0, this._cvGraph.width, this._cvGraph.height);

	        const maxRX = this._maxRX === null ? data.length-1 : this._maxRX;
	        const rangeX = maxRX - this._minRX;
	        const marginLeft = this.config.marginLeft;
	        const marginTop  = this.config.marginTop;

	        // Generates line with image data from FFT data
	        // and draw this line at the top of the graph.
	        const imgLine = ctx.createImageData(this._cvGraph.width, 1);
	        const imgData = imgLine.data;
	        for (var cx = 0; cx < width; cx++) {
	            const idxData =  Math.floor(cx/width * rangeX);
	            const color = this._colorScale(data[idxData]).rgb();
	            const idx = cx * 4;
	            imgData[idx]     = color[0];
	            imgData[idx + 1] = color[1];
	            imgData[idx + 2] = color[2];
	            imgData[idx + 3] = 255;
	        }
	        ctx.putImageData(imgLine, marginLeft, marginTop);

	        // Copy the auxiliary canvas over the main canvas one line down
	        ctx.drawImage(this._cvHidden, 0, 0, this._cvHidden.width, this._cvHidden.height, 0, 1, this._cvGraph.width, this._cvGraph.height);
	        ctx.restore();
	    },

	    // Draws the cursor on the cursor canvas which is overlaid over the graph canvas
	    _drawCursor: function() {

	        // Clears the canvas to transparent
	        const ctx = this._ctxCursor;
	        ctx.clearRect(0, 0, this._cvCursor.width, this._cvCursor.height);

	        if (!this._vertCur.visible || !this._vertCur.enabled) {
	            return;
	        }

	        // Draws the vertical cursor line
	        const width = this._cvCursor.width - this.config.marginRight - this.config.marginLeft;
	        const height = this._cvCursor.height - this.config.marginTop - this.config.marginBottom;
	        const cx = (this._vertCur.pos + 0.5) * width;
	        ctx.strokeStyle = this._vertCur.color;
	        ctx.lineWidth = this._vertCur.width;
	        ctx.beginPath();
	        const posX = this.config.marginLeft + cx;
	        ctx.moveTo(posX, this.config.marginTop);
	        ctx.lineTo(posX, this.config.marginTop + height);
	        ctx.stroke();

	        // Draws frequency text
	        //const freq = this._centerFrequency + this._vertCur.pos * this._bandwidth;
	        //ctx.textAlign = 'left';
	        //ctx.fillText(this._formatFreq(freq), posX + 4, this.config.marginTop + 10);
	    },

	    _draw2: function() {

	        this._ctxCursor.fillStyle = '#ff000002';
	        this._ctxCursor.fillRect(0, 0, this._cvCursor.width, this._cvCursor.height);

	        if (this._data.length == 0) {
	            return;
	        }
	        const width = this._canvas.width - this.config.marginRight - this.config.marginLeft;
	        const data = this._data;

	        // Copy auxiliary hidden canvas to itself one line down
	        const ximage = this._ctxHidden.getImageData(0, 0, this._canvas.width, this._canvas.height-1);
	        this._ctxHidden.putImageData(ximage, 0, 1);

	        const maxRX = this._maxRX === null ? data.length-1 : this._maxRX;
	        const rangeX = maxRX - this._minRX;
	        const marginLeft = this.config.marginLeft;
	        const marginTop  = this.config.marginTop;

	        // Generates line with image data from FFT data
	        // and draw this line at the top of the graph.
	        const imgLine = this._ctxHidden.createImageData(this._cvHidden.width, 1);
	        const imgData = imgLine.data;
	        for (var cx = 0; cx < width; cx++) {
	            const idxData =  Math.floor(cx/width * rangeX);
	            const color = this._colorScale(data[idxData]).rgb();
	            const idx = cx * 4;
	            imgData[idx]     = color[0];
	            imgData[idx + 1] = color[1];
	            imgData[idx + 2] = color[2];
	            imgData[idx + 3] = 255;
	        }
	        this._ctxHidden.putImageData(imgLine, marginLeft, marginTop);
	        // Copy the auxiliary canvas over the main canvas
	        this._ctx.drawImage(this._cvHidden, 0, 0);
	    },


	    // Returns mouse position relative to the canvas for the specified event
	    _mousePos: function(ev, cv) { 
	       
	        const rect = cv.getBoundingClientRect();
	        return {
	            x: ev.clientX - rect.left,
	            y: ev.clientY - rect.top
	        };
	    },

	    // Called when the mouse cursor moves over the cursor canvas
	    _onMouseMove: function(ev) {

	        const pos = this._mousePos(ev, this._cvCursor);
	        //console.log('mouse', pos, this.config.marginLeft);
	        const width = this._cvCursor.width - this.config.marginRight - this.config.marginLeft;
	        const height = this._cvCursor.height - this.config.marginTop - this.config.marginBottom;
	        if (this._vertCur.enabled) {
	            if (pos.x < this.config.marginLeft || pos.x > this.config.marginLeft + width) {
	                this._vertCur.visible = false;
	                return;
	            }
	            if (pos.y < this.config.marginTop || pos.y > this.config.marginTop + height) {
	                this._vertCur.visible = false;
	                return;
	            }
	            this._vertCur.visible = true;
	            let px = (pos.x - this.config.marginLeft) / width - 0.5;
	            this.setVertCurPos(px);
	        }

	    },

	    // Called when the mouse is clicked over the cursor canvas
	    _onMouseClick: function(/* ev */) {

	    },

	    // Called when the mouse exits from the cursor canvas
	    _onMouseOut: function(/* ev */) {

	        this._vertCur.visible = false;
	        this._redrawCursor = true;
	    }

	}, webix.ui.view, webix.EventSystem);

	function toolbar(app) {
	    // Builds supported radios select options
	    const radiosList = [];
	    for (var radio in app._server.radios) {
	        radiosList.push({id: radio, value: app._server.radios[radio]});
	    }
	    return {
	        view: 'toolbar',
	        height: 54,
	        elements: [
	            { view: 'button',
	                type:  'icon',
	                id:    'btnMenu',
	                icon:  Menu,
	                width: 48
	            },
	            { view: 'label',
	                label: `GoRad ${app.version}/${app._server.version}`,
	                width: 132,
	            },
	            { view:'select',
	                id:      'selRadio',
	                label:   '',
	                width:   200,
	                options: radiosList,
	            },
	            {view: 'icon',
	                id:      'btnConfig',
	                icon:    Settings,
	                tooltip: 'Radio configuration',
	                width:   48
	            },
	            { view: 'icon',
	                id:      'btnStartStop',
	                icon:    Play,
	                tooltip: 'Start/stop radio',
	                width:   48
	            },
	            { view: 'icon',
	                id:      'btnMute',
	                icon:    VolumeHigh,
	                tooltip: 'Mute audio',
	                width:   48
	            },
	            { view: 'slider_wheel',
	                type:    'alt',
	                id:      'sliderVol',
	                label:   '', 
	                value:   80,
	                width:   180,
	                title:   webix.template('Vol: #value#%'),
	                tooltip: 'Audio volume',
	            },
	            { view: 'icon',
	                id:      'btnTuningType',
	                icon:    CenterTuning,
	                tooltip: 'Center tuning / Free tuning',
	                width:   48
	            },
	            { view:      'tuner',
	                id:      'tuner1',
	                digits:  10,
	                value:   89000000,
	                width:   300,
	                tooltip: 'Radio tuner frequency',
	            },
	        ]
	    };
	}

	const leftpane = {
	    view: 'scrollview',
	    width: 200,
	    body: {
	        view: 'accordion',
	        multi: true,
	        scroll: 'y',
	        rows: [
	            { header: 'radio'},
	            { header: 'radio2'},
	            { header: 'radio3'},
	            { header: 'radio4'},
	            { header: 'radio5'},
	            { header: 'radio6'},
	            { header: 'radio6'},
	            { header: 'radio6'},
	            { header: 'radio6'},
	            { header: 'radio6'},
	            { header: 'radio6'},
	            { header: 'radio6'},
	            { header: 'radio8'},
	            { header: 'radio9'},
	        ],
	    },
	};


	// Builds main screen
	function buildScreen(app) {

	    webix.ui({
	        view: 'layout',
	        id:   'mainLayout',
	        rows:[
	            toolbar(app),
	            { cols: [
	                leftpane,
	                { view: 'resizer' },
	                { view: 'layout',
	                    rows: [
	                        { view: 'fftgraph', id: 'fftgraph1' },
	                        { view: 'resizer' },
	                        { view: 'waterfall', id: 'waterfall1' },
	                    ]
	                }
	            ]},
	        ],
	    });
	}

	class EventBus {

	    constructor() {
	        this._events = {};
	    }

	    // Register listener for event
	    listen(evname, cb, ctx = null) {

	        var subs = this._events[evname];
	        if (subs === undefined) {
	            subs = [];
	            this._events[evname] = subs;
	        }
	        subs.push({cb, ctx});
	    }

	    // Unregister listener for the specified event name and callback
	    // Returns the number of listeners found and unregistered
	    unlisten(evname, cb) {
	        var found = 0;
	        const subs = this._events[evname];
	        if (subs === undefined) {
	            return found;
	        }
	        var i = 0;
	        while (i < subs.length) {
	            if (subs[i].cb != cb) {
	                i++;
	                continue;
	            }
	            subs.splice(i, 1);
	            found++;
	        }
	        return found;
	    }

	    // Emits event calling all registered listeners
	    emit(evname, ...args) {
	        const subs = this._events[evname];
	        if (subs === undefined) {
	            return;
	        }
	        for (var i = 0; i < subs.length; i++) {
	            const sub = subs[i];
	            if (sub.ctx !== null) {
	                sub.call(sub.ctx, sub.cb(args));
	            } else {
	                sub.cb(args);
	            }
	        }
	    }
	}

	const OPEN = 1;
	const CLOSED = 3;

	// Generated events
	const ON_OPENED = 'opened';
	const ON_CLOSED = 'closed';
	const ON_ERROR  = 'error';
	const ON_RXMSG  = 'rxmsg';


	//
	// WebSocket channel class
	//
	class Channel extends EventBus {

	    constructor() {
	        super();
	        this._socket = null;    // WebSocket object
	        this._nextid = 1;       // ID for next request
	        this._resp = {};        // Expected responses by id
	        this._bincb = null;     // Callback for received binary messages
	    }

	    // Try to connect to the server using the specified relative url.
	    // Calls the specified callback with the connection result
	    connect(relurl, cb) {

	        // Close current connection if necessary
	        if (this._socket && this._socket.readyState != CLOSED) {
	            this._socket.close();
	            this._socket = null;
	        }
	        
	        // Open web socket connection with server using the supplied relative URL
	        const url = 'ws://' + document.location.host + relurl;
	        this._socket = new WebSocket(url);
	        this._socket.binaryType = 'arraybuffer';

	        // Sets initial event handlers for connection callback
	        this._socket.onerror = (ev) => cb('error');
	        this._socket.onclose = (code, reason) => cb('closed');

	        // When connection is opened sets normal event handlers
	        this._socket.onopen = () => {
	            this._socket.onerror = (ev) => this._onError(ev);
	            this._socket.onclose = (code, reason) => this._onClose(code, reason);
	            this.emit(ON_OPENED);
	            cb('');   
	        };
	        this._socket.onmessage = (ev) => this._onMessage(ev);
	    }

	    close() {
	        if (this._socket.readyState != CLOSED) {
	            return;
	        }
	        this._socket.close();
	    }

	    // Returns the socket ready state
	    status() {
	        if (!this._socket) {
	            return CLOSED;
	        }
	        return this._socket.readyState;
	    }

	    // Sends message with name and data
	    // The optional callback will be called when a response for
	    // this message is received.
	    send(name, data, cb=null) {

	        // Checks if connection is open
	        if (this._socket.readyState != OPEN) {
	            if (cb) {
	                cb({err:'connection is not open'});
	            }
	            return;
	        }

	        // Builds message envelope
	        const msg = {
	            id:   this._nextid,
	            name: name,
	            data: JSON.stringify(data),
	        };
	        this._nextid++;

	        // If callback supplied, waits response of this message
	        if (cb) {
	            this._resp[msg.id] = {cb};
	        }

	        // Encodes message to JSON and sends is as text
	        const text = JSON.stringify(msg);
	        this._socket.send(text);
	    }

	    // Sets callback to call when binary messages are received
	    onBinary(cb=null) {

	        this._bincb = cb;
	    }

	    // Called when a message is received from the socket.
	    // The message can be a response to a previous sent request or a
	    // message initiated by the server.
	    _onMessage(ev) {

	        // Messages of type string are JSON encoded
	        if (typeof(ev.data) == 'string') {
	            //console.log('_onMessage:', ev.data);
	            const msg = JSON.parse(ev.data);
	            // If message id is not present or id==0, emits an event with this message
	            if (!msg.id) {
	                this.emit(ON_RXMSG);
	                return;
	            }
	            // Checks if this message is response from a previous request
	            const resp = this._resp[msg.id];
	            if (resp) {
	                resp.cb(msg);
	                delete this._resp[msg.id];
	                return;
	            }
	            console.log('Received response without request');
	            return;
	        }

	        // Messages of type binary (ArrayBuffer)
	        if (this._bincb) {
	            this._bincb(ev.data);
	        }
	    }

	    _onClose(code, reason) {
	        console.log('_onClose:', code, reason);
	        this.emit(ON_CLOSED);
	    }

	    _onError(ev) {
	        console.log('_onError:', this, ev);
	        this.emit(ON_ERROR);
	    }
	}

	// Prefix for all window ids
	const winPrefix = 'rtlsdr.win';


	// Prefix for all form ids
	const formPrefix = 'rtlsdr.form';

	// Current setup configuration
	var config = {
	    radioid:        '0',
	    samplerate:     2400000,
	    freqcorrection: 0,
	    enableagc:      true,
	};

	createFormSetup();

	//
	// Creates form
	//
	function createFormSetup() {

	    webix.ui({
	        view:       'window',
	        id:         `${winPrefix}.setup`,
	        modal:      true,
	        move:       true,
	        position:   'center',
	        head:       'RTL-SDR',
	        width:      400,
	        body: {
	            view:   'form',
	            id:     `${formPrefix}.setup`,
	            scroll: false,
	            elements: [
	                {view: 'counter',
	                    id:         `${formPrefix}.radioid`,
	                    label:      'Radio index:',
	                    name:       'radioid',
	                },
	                {view: 'richselect',
	                    id:         `${formPrefix}.samplerate`,
	                    label:      'Sample rate (Hz):',
	                    name:       'samplerate',
	                    value:      'all',
	                    options:    [
	                        {id:3200000, value: '3,200,000'},
	                        {id:3000000, value: '3,000,000'},
	                        {id:2800000, value: '2,800,000'},
	                        {id:2400000, value: '2,400,000'},
	                        {id:1920000, value: '1,920,000'},
	                        {id:1800000, value: '1,800,000'},
	                    ],
	                },
	                {view: 'counter',
	                    id:         `${formPrefix}.freqcorrection`,
	                    label:      'Frequency correction (PPM):',
	                    name:       'freqcorrection',
	                },
	                {view: 'checkbox',
	                    id:         `${formPrefix}.enableagc`,
	                    label:      'Enable AGC:',
	                    name:       'enableagc',
	                },
	                {margin: 5, cols: [
	                    {view: 'button',
	                        value:      'OK',
	                        type:       'form',
	                        click:      () => $$(`${formPrefix}.setup`).app.submit(),
	                    },
	                    {view: 'button',
	                        value:      'Cancel',
	                        type:       'form',
	                        click:      () => $$(`${winPrefix}.setup`).hide(),
	                    },
	                ]},
	            ],
	            elementsConfig:{
	                labelAlign: 'right',
	                labelWidth: 200,
	            },
	        }
	    });

	    // Sets form methods
	    $$(`${formPrefix}.setup`).app = {

	        // Show setup form
	        show: function () {
	            $$(`${formPrefix}.setup`).setValues(config);
	            $$(`${winPrefix}.setup`).show();
	        },
	       
	        // Submit setup form
	        submit: function() {

	            // Get and saves configuration
	            var values = $$(`${formPrefix}.setup`).getValues();
	            config = values;
	            config.radioid = config.radioid.toString();
	            console.log(values);
	            // Hides the setup window
	            $$(`${winPrefix}.setup`).hide();
	        },

	        // Returns radio setup configuration
	        getConfig: function() {
	            return config;
	        }
	    };
	}

	//
	// Application class
	//
	class App {


	    constructor() {
	        this.version = '1.2.3';
	        this._cmdSock = new Channel();
	        this._fftSock = new Channel();
	        //this.evbus = new EventBus();
	        this._graphs = [];
	        this._audioMute = false;
	        this._centerTuning = true;
	        this._server = null;
	        this._started = false;
	        this._renderGraphs = this._renderGraphs.bind(this);
	        this._log = getLogger('App');
	        this._centerFrequency = 0;
	        this._filterFrequency = 0;
	        this._bandwidth = 0;
	        this._textDecoder = new TextDecoder('utf-8');
	    }

	    // Start rendering the graphs by requestAnimationFrame
	    _renderGraphs() {
	        for (var i = 0; i < this._graphs.length; i++) {
	            this._graphs[i].render();
	        }
	        window.requestAnimationFrame(this._renderGraphs);
	    }

	    // Try to connect to server command socket.
	    // Then connect to server FFT socket.
	    connect() {
	        this._cmdSock.connect('/ws', (err) => {
	            if (err)  {
	                webix.alert({
	                    title:      'ERROR',
	                    text:       'Error connection with server Command WebSocket',
	                    ok:         'Retry',
	                    callback:   () => this.connect(),
	                });
	                return;
	            }
	            this.connectFFT();
	        });
	    }

	    // Try to connect to server FFT socket.
	    // Then get configuration.
	    connectFFT() {
	        this._fftSock.connect('/fft', (err) => {
	            if (err)  {
	                webix.alert({
	                    title:      'ERROR',
	                    text:       'Error connection with server FFT WebSocket',
	                    ok:         'Retry',
	                    callback:   () => this.connectFFT(),
	                });
	                return;
	            }
	            // Sets callback when receiving binary data from this socket
	            this._fftSock.onBinary((data) => this.onRxFFT(data));
	            // Continue with get configuration
	            this.getConfig();
	        });
	    }

	    // Sends request to get server configurations
	    getConfig() {
	        this._cmdSock.send('getconfig', null, (resp) => {
	            this._server = resp.data;
	            this.setup();
	        });
	    }

	    setup() {
	        // Build screen
	        buildScreen(this);

	        // Sets selected radio
	        $$('selRadio').setValue(this._server.radiodef);

	        // Attach event handlers to ui controls
	        $$('btnConfig').attachEvent('onItemClick', () => this.configRadio());
	        $$('btnStartStop').attachEvent('onItemClick', () => this.onStartStop());
	        //$$('btnStop').attachEvent('onItemClick', () => this.stop());
	        $$('btnMute').attachEvent('onItemClick', () => this.audioMute());
	        $$('sliderVol').attachEvent('onChange', () => this.audioVolume());
	        $$('tuner1').attachEvent('onChange', (f) => this.onTunerControl(f));
	        $$('btnTuningType').attachEvent('onItemClick', () => this.onTuningType());

	        // Sets enabled states
	        $$('selRadio').enable();
	        $$('btnConfig').enable();
	        $$('btnStartStop').enable();
	        //$$('btnStop').disable();
	        $$('tuner1').disable();

	        // Attach event handler to fftgraph
	        $$('fftgraph1').attachEvent('onFrequency', (f) => this.onFrequencyCursor(f));

	        // Sets graphs to render
	        this._graphs.push($$('fftgraph1'));
	        this._graphs.push($$('waterfall1'));
	        this._renderGraphs();
	    }

	    // Show radio configuration window
	    configRadio() {
	        const radioType = $$('selRadio').getValue();
	        const formid = radioType + '.form.setup';
	        this._log.debug('config radio ==========> %s', formid);
	        $$(formid).app.show();
	    }

	    // Starts the selected radio
	    start() {

	        const radioType = $$('selRadio').getValue();
	        const formid = radioType + '.form.setup';
	        var config = $$(formid).app.getConfig();
	        config.radiotype = radioType;
	        this._cmdSock.send('start', config, (resp) => {
	            if (resp.err) {
	                webix.alert({
	                    title:  'ERROR',
	                    text:   resp.err,
	                    ok:     'OK',
	                });
	                $$('btnStartStop').enable();
	                return;
	            }

	            // Sets initial frequency
	            let frequency = this._centerFrequency == 0 ? 89100000 : this._centerFrequency;
	            $$('tuner1').setValue(frequency);

	            // Update controls
	            $$('selRadio').disable();
	            $$('btnConfig').disable();
	            $$('btnStartStop').enable();
	            $$('btnStartStop').define('icon', Stop);
	            $$('btnStartStop').refresh();
	            this._started = true;
	        });
	    }

	    // Stops the radio
	    stop() {

	        // Sends command to server
	        this._cmdSock.send('stop', null, (resp) => {
	            if (resp.err) {
	                webix.alert({
	                    title:  'ERROR',
	                    text:   resp.err,
	                    ok:     'OK',
	                });
	            }
	        });

	        // Update controls
	        $$('selRadio').enable();
	        $$('btnConfig').enable();
	        $$('tuner1').disable();
	        $$('btnStartStop').enable();
	        $$('btnStartStop').define('icon', Play);
	        $$('btnStartStop').refresh();
	        this._started = false;
	    }

	    // Toggle radio start / stop
	    onStartStop() {

	        $$('btnStartStop').disable();
	        if (!this._started) {
	            this.start();
	        } else {
	            this.stop();
	        }
	    }

	    // Toggle audio mute
	    audioMute() {

	        this._audioMute = !this._audioMute;
	        var icon = VolumeMute;
	        if (!this._audioMute) {
	            icon = VolumeHigh;
	        }
	        $$('btnMute').define('icon', icon);
	        $$('btnMute').refresh();

	        this._cmdSock.send('audiomute', {mute: this._audioMute}, (resp) => {
	            this._log.debug('audiomute resp: %s', resp);    
	            if (resp.err) {
	                return;
	            }
	        });
	    }

	    // Set audio volume
	    audioVolume() {

	        const val = $$('sliderVol').getValue();
	        this._cmdSock.send('audiovol', {vol: val / 100}, (resp) => {
	            this._log.debug('audiovol resp: %s', resp);    
	            if (resp.err) {
	                return;
	            }
	        });
	    }

	    // Called when tuner control frequency changes
	    onTunerControl(freq) {

	        this.sendFrequency(freq, 0);
	    }

	    // Called when tuning type button is clicked
	    onTuningType() {

	        this._centerTuning = !this._centerTuning;
	        var icon = CenterTuning;
	        if (!this._centerTuning) {
	            icon = FreeTuning;
	        }
	        $$('btnTuningType').define('icon', icon);
	        $$('btnTuningType').refresh();
	    }

	    // Called when the frequency cursor in the FFT graph is clicked
	    onFrequencyCursor(freq) {

	        this.sendFrequency(freq, 0);
	    }

	    // Called with received FFT header and data (ArrayBuffer) from server
	    onRxFFT(data) {

	        const minDataSize = 512;
	        // Checks if header was received
	        if (data.byteLength < minDataSize) {
	            // Decodes ArrayBuffer with bytes in UTF8 to string
	            //let decoder = new TextDecoder('utf-8');
	            let decoded = this._textDecoder.decode(data);
	            // Parse decoded JSON string
	            const header = JSON.parse(decoded);
	            //console.log('header', header);
	            // Updates center frequency if necessary
	            if (this._centerFrequency != header.CenterFrequency) {
	                this._centerFrequency = header.CenterFrequency;
	                $$('tuner1').setValue(this._centerFrequency, false);
	                $$('fftgraph1').setCenterFrequency(this._centerFrequency);
	                console.log('updated center frequency:', this._centerFrequency);
	            }
	            // Updates filter frequency if necessary
	            if (this._filterFrequency != header.FilterFrequency) {
	                this._filterFrequency = header.FilterFrequency;
	                console.log('updated filter frequency:', this._filterFrequency);
	            }
	            // Updates bandwidth if necessary
	            if (this._bandwidth != header.Bandwidth) {
	                this._bandwidth =  header.Bandwidth;
	                $$('fftgraph1').setBandwidth(this._bandwidth);
	                console.log('updated bandwidth:', this._bandwidth);
	            }
	        // Otherwise it is FFT data
	        } else {
	            const buf = new Float32Array(data);
	            $$('fftgraph1').setData(buf);
	            $$('waterfall1').setData(buf);
	        }
	    }

	    // Sends command to set the receiver center and filter frequencies.
	    sendFrequency(centerFreq, filterFreq) {

	        let params = {
	            CenterFreq: Math.floor(centerFreq),
	            FilterFreq: filterFreq,
	        };
	        this._cmdSock.send('setfreq', params, (resp) => {
	            this._log.debug('setfreq resp: %s', resp);
	        });
	    }
	}

	// Creates logger
	const log = getLogger('MAIN');
	const hconsole = createConsoleHandler({
	    level:      DEBUG,
	    useColors:  true
	});
	log.addHandler(hconsole);
	log.setLevel('DEBUG');
	log.info('Starting...');

	//
	// Creates application instance and starts it
	//
	const app = new App();
	app.connect();

}());
