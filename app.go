package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"sync"
)

// App is the main application object
type App struct {
	config  AppConfig // application configuration
	mut     sync.Mutex
	server  *http.Server // http server
	cmdSock *Socket      // command Websocket handler
	fftSock *Socket      // fft Websocket handler
	rx      *Receiver    // Receiver (dsp graph)
}

// App config describes the application initial configuration
type AppConfig struct {
	address string // Listening address <host>:<port>
	clipath string // Path for client path (index.html)
}

// StartReq is the type for client request to start receiver
type StartReq struct {
	RadioType  string `json:"radiotype"`
	RadioId    string `json:"radioid"`
	Samplerate int    `json:"samplerate"`
}

// SetFrequencyReq is the type for client request to set radio frequency
type SetFrequencyReq struct {
	CenterFreq int     // Tuner center frequency
	TunerFreq  float64 // Filter frequency offset from center
}

type AudioMuteReq struct {
	Mute bool
}

type AudioVolumeReq struct {
	Vol float32
}

// ConfigResp is the server response for client "getconfig" request
type ConfigResp struct {
	Version  string            `json:"version"`  // Server version
	Radios   map[string]string `json:"radios"`   // Supported radios
	RadioDef string            `json:"radiodef"` // Default radio selected
}

const (
	GetConfigMsg = "getconfig"
)

// NewApp creates and returns an application object for the specified configuration
func NewApp(cfg *AppConfig) *App {

	app := new(App)
	app.config = *cfg
	return app
}

// Start starts the application http server and blocks
func (app *App) Start() error {

	app.mut.Lock()
	app.server = &http.Server{Addr: app.config.address}
	app.mut.Unlock()

	// Creates static file server handler
	//fs := http.FileServer(http.Dir("/home/leonel/go/src/gitlab.com/leonsal/gorad/wroot"))
	fs := http.FileServer(assetFS())
	http.Handle("/", fs)

	// Creates command WebSocket handler and register message handlers
	app.cmdSock = NewSocket()
	app.cmdSock.Register("getconfig", app.onGetConfigReq)
	app.cmdSock.Register("start", app.onStartReq)
	app.cmdSock.Register("stop", app.onStopReq)
	app.cmdSock.Register("audiomute", app.onAudioMuteReq)
	app.cmdSock.Register("audiovol", app.onAudioVolumeReq)
	app.cmdSock.Register("setfreq", app.onSetFrequencyReq)
	http.HandleFunc("/ws", app.cmdSock.Handler)

	// Creates fft WebSocket handler
	app.fftSock = NewSocket()
	http.HandleFunc("/fft", app.fftSock.Handler)

	// Starts HTTP/WebSocket server
	return app.server.ListenAndServe()
}

// Stop stops the aplication http server
func (app *App) Stop() {

	app.mut.Lock()
	if app.server != nil {
		app.server.Shutdown(nil)
	}
	app.mut.Unlock()
}

// onGetConfigReq process client request to return server configuration
func (app *App) onGetConfigReq(s *Socket, msg *Message) {

	resp := ConfigResp{
		Version: "0.1.2",
		Radios: map[string]string{
			"rtlsdr":  "RTL-SDR",
			"airspy":  "Air Spy",
			"sdrplay": "SDR Play",
		},
		RadioDef: "rtlsdr",
	}
	s.SendResponse(msg.Id, "", &resp)
}

// onStartReq process received "start" message
func (app *App) onStartReq(s *Socket, msg *Message) {

	// Decodes received request
	var sr StartReq
	err := json.Unmarshal([]byte(msg.Data), &sr)
	fmt.Printf("start-------------------------------------->:%+v\n", sr)
	if err != nil {
		s.SendResponse(msg.Id, err.Error(), nil)
	}

	// Checks if receiver already started
	if app.rx != nil {
		s.SendResponse(msg.Id, "Receiver is already started", nil)
		return
	}

	// Creates new receiver for specified radio type
	rx, err := NewReceiver(sr.RadioType, sr.RadioId)
	if err != nil {
		s.SendResponse(msg.Id, err.Error(), nil)
		return
	}
	app.rx = rx

	// Sets up sample rate
	err = app.rx.SetSampleRate(sr.Samplerate)
	if err != nil {
		s.SendResponse(msg.Id, err.Error(), nil)
		return
	}

	// Sets the channel to stream FFT data to client
	app.rx.StreamFFT(app.fftSock.TxBuffer())

	// Starts receiver
	err = app.rx.Start()
	if err != nil {
		s.SendResponse(msg.Id, err.Error(), nil)
	} else {
		s.SendResponse(msg.Id, "", nil)
	}

}

// onStopReq process received "stop" message
func (app *App) onStopReq(s *Socket, msg *Message) {

	// Checks if receiver already stopped
	if app.rx == nil {
		s.SendResponse(msg.Id, "Receiver is already stopped", nil)
		return
	}

	// Stops streaming FFT data to client
	//app.rx.StreamFFT(nil)

	// Closes receiver
	err := app.rx.Close()
	app.rx = nil
	if err != nil {
		s.SendResponse(msg.Id, err.Error(), nil)
	} else {
		s.SendResponse(msg.Id, "", nil)
	}
}

// onAudioMuteReq process received "audiomute" messages
func (app *App) onAudioMuteReq(s *Socket, msg *Message) {

	//// Decodes message
	//var mdec AudioMuteReq
	//err := json.Unmarshal([]byte(msg.Data), &mdec)
	//if err != nil {
	//	log.Println("error: %v decoding 'audiomute' message", err)
	//	return
	//}

	////Sets radio audio mute
	//err = app.radio.SetAudioMute(mdec.Mute)
	//if err != nil {
	//	s.SendResponse(msg.Id, err.Error(), nil)
	//} else {
	//	s.SendResponse(msg.Id, "", nil)
	//}
}

// onAudioVolumeReq process received "audiovol" messages
func (app *App) onAudioVolumeReq(s *Socket, msg *Message) {

	//// Decodes message
	//var mdec AudioVolumeReq
	//err := json.Unmarshal([]byte(msg.Data), &mdec)
	//if err != nil {
	//	log.Println("error: %v decoding 'audiovol' message", err)
	//	return
	//}

	//// Sets radio audio volume
	//err = app.radio.SetAudioVolume(mdec.Vol)
	//if err != nil {
	//	s.SendResponse(msg.Id, err.Error(), nil)
	//} else {
	//	s.SendResponse(msg.Id, "", nil)
	//}
}

// onSetFrequencyReq process received "setfreq" message
// which sets the radio tuner center frequency and the filter frequency.
func (app *App) onSetFrequencyReq(s *Socket, msg *Message) {

	// Decodes message
	var mdec SetFrequencyReq
	err := json.Unmarshal([]byte(msg.Data), &mdec)
	if err != nil {
		log.Printf("error: %v decoding 'setfreq' message\n", err)
		return
	}
	log.Printf("onSetFrequencyReq:%v", mdec)

	if app.rx == nil {
		s.SendResponse(msg.Id, "Receiver not started", nil)
		return
	}

	// Sets radio tuner center frequency
	err = app.rx.SetCenterFrequency(mdec.CenterFreq)
	if err != nil {
		s.SendResponse(msg.Id, err.Error(), nil)
		return
	} else {
		s.SendResponse(msg.Id, "", nil)
	}

	// Sets filter frequency
}
