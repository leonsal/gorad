package main

import (
	"encoding/json"
	"fmt"
	"strconv"

	"gitlab.com/leonsal/godsp/core"
	"gitlab.com/leonsal/godsp/fourier"
	"gitlab.com/leonsal/godsp/radio"
	"gitlab.com/leonsal/godsp/radio/rtlsdr"
	"gitlab.com/leonsal/godsp/util"
)

type RxModulation int

const (
	RxAM RxModulation = 0
	RxFM RxModulation = 1
)

// Receiver implements a radio receiver using an SDR
type Receiver struct {
	radioType       string           // SDR Radio type
	frameSize       int              // Frame size in number of samples
	sampleRate      int              // Current sample rate (Hz)
	centerFrequency int              // Current center frequency (Hz)
	filterFrequency float64          // Current filter frequency relative to center +/- bandwith
	dev             radio.IRadio     // Radio device
	inpFrequency    *core.Input      // Radio frequency input
	fftPSD          *fourier.FFT2PSD // Converter for FFT output to Power Spectrum Density
	fftMagChan      *util.Channel    // Node to read fft magnitude and send to channel
	chControl       chan struct{}    // Control channel for readFFT goroutine
	chFFT           chan interface{} // Channel passed to fftMagChan to write data
	chStreamFFT     chan interface{}
}

// FFTHeader describes the FFT data sent to the client
type FFTHeader struct {
	CenterFrequency float64
	FilterFrequency float64
	Bandwidth       float64
}

// NewReceiver creates and returns a pointer to a new receiver
// using the specified radio type.
func NewReceiver(radioType string, radioId string) (*Receiver, error) {

	rx := new(Receiver)
	rx.radioType = radioType
	rx.frameSize = 1 * (1 << 13) // Multiple of 8Kbytes
	rx.centerFrequency = 89100000

	switch radioType {
	case "rtlsdr":
		// Get radio index by its id string
		index, err := strconv.Atoi(radioId)
		if err != nil {
			return nil, fmt.Errorf("Invalid Radio ID")
		}
		// Default sample rate
		rx.sampleRate = 2400000
		// Initializes radio configuration
		cfg := rtlsdr.Config{
			Index:          uint(index),
			Framesize:      rx.frameSize,
			Samplerate:     rx.sampleRate,
			Frequency:      rx.centerFrequency,
			FreqCorrection: 0,
			EnableAGC:      true,
		}
		// Creates and opens radio device
		dev, err := rtlsdr.New(&cfg)
		if err != nil {
			return nil, err
		}
		rx.dev = dev
	case "airspy":
		return nil, fmt.Errorf("Not supported yet")
	case "sdrplay":
		return nil, fmt.Errorf("Not supported yet")
	default:
		panic("Invalid radio type")
	}

	// Creates Rater and connects radio device output to it
	rater := util.NewRater(core.VComplex, 30, rx.frameSize)
	rx.dev.Connect(rater.Input("data"))

	// Create FFT node which generates normalized power spectrum output in db.
	// Assumes the maximum peak to peak amplitude of the input signal is 2.0
	rx.fftPSD = fourier.NewFFT2PSD(1.0, rx.frameSize, 5)
	rx.fftPSD.Input("window").Write(uint(fourier.BlackmanHarris))
	rater.Connect(rx.fftPSD.Input("data"))

	// Creates channels for transfering fft data to socket
	rx.chControl = make(chan struct{})
	rx.chFFT = make(chan interface{})

	// Creates channel Node to receive FFT PSD
	rx.fftMagChan = util.NewChannel(core.VFloat, rx.chFFT, 0, rx.frameSize)
	rx.fftPSD.Connect(rx.fftMagChan.Input("data"))

	rx.inpFrequency = rx.dev.Input("frequency")

	return rx, nil
}

// RadioType returns this receiver radio type
func (rx *Receiver) RadioType() string {

	return rx.radioType
}

// SetSampleRate sets the receiver sample rate
func (rx *Receiver) SetSampleRate(srate int) error {

	rx.sampleRate = srate
	return rx.dev.SetSampleRate(srate)
}

// SetCenterFrequency sets the receiver tuner frequency
func (rx *Receiver) SetCenterFrequency(freq int) error {

	rx.centerFrequency = freq
	fmt.Println("Receiver SetFrequency 0")
	rx.inpFrequency.Write(freq)
	fmt.Println("Receiver SetFrequency 1")
	return nil
}

// SetModulation sets the receiver demodulation
func (rx *Receiver) SetModulation(mod RxModulation) error {

	return nil
}

// Start starts the receiver
func (rx *Receiver) Start() error {

	err := rx.dev.Start()
	if err != nil {
		return err
	}
	go rx.readFFT()
	return nil
}

// Stop stops the receiver
func (rx *Receiver) Stop() error {

	rx.chControl <- struct{}{}
	err := rx.dev.Stop()
	return err
}

// Close stops and closes the receiver
func (rx *Receiver) Close() error {

	rx.chControl <- struct{}{}
	err := rx.dev.Close()
	rx.dev = nil
	return err
}

func (r *Receiver) SetAudioMute(mute bool) error {

	return nil
}

func (r *Receiver) SetAudioVolume(vol float32) error {

	return nil
}

func (rx *Receiver) StreamFFT(ch chan interface{}) {

	rx.chStreamFFT = ch
}

// readFFT runs as a goroutine and reads the channel with FFT magnitude data
// Then sends a header and the data to the target channel (rx.chStreamFFT).
func (rx *Receiver) readFFT() {

	for {
		select {
		// Control channel: If anything received terminates this goroutine
		case <-rx.chControl:
			return
		// FFT data channel
		case data := <-rx.chFFT:
			if data == nil {
				return
			}
			// Builds and sends JSON encoded FFT header to client
			header := FFTHeader{
				CenterFrequency: float64(rx.inpFrequency.Read().(uint)),
				FilterFrequency: float64(rx.filterFrequency),
				Bandwidth:       float64(rx.sampleRate),
			}
			encHeader, err := json.Marshal(header)
			if err != nil {
				panic(err)
			}
			rx.chStreamFFT <- encHeader

			// Sends FFT data to client
			rx.chStreamFFT <- data
		}
	}
}
