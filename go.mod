module gitlab.com/leonsal/gorad

require (
	github.com/gorilla/websocket v1.4.0
	gitlab.com/leonsal/godsp v0.0.0-20190225182833-36346b7090d9
	golang.org/x/exp v0.0.0-20190221220918-438050ddec5e // indirect
	golang.org/x/net v0.0.0-20190225153610-fe579d43d832 // indirect
	golang.org/x/sync v0.0.0-20181221193216-37e7f081c4d4 // indirect
	golang.org/x/tools v0.0.0-20190221204921-83362c3779f5 // indirect
	gonum.org/v1/gonum v0.0.0-20190221132855-8ea67971a689 // indirect
)
